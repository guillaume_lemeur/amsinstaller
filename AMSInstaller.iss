#define MyAppName "AMS" 
#define MyAppPublisher "Aircraft Maintenance Systems Rd Inc."
#define MyAppURL "http://www.aircraftms.com"
#define UninsHs_BackupDir "{localappdata}\AMS"
; exe Files
#define AccountManagerExe "AccountManager.exe"
#define AmmExe "AircraftManager.exe"
#define ADGetExe "ADGet.exe"
#define ADTrackerExe "ADTracker.exe" 
#define AirKeeperExe "AirKeeper.exe"
#define BaseManageExe "BasesManager.exe"
#define CWRExe "ComponentWorkReport.exe"
#define ESExe "ElectronicSignature.exe"
#define InvManagerEXE "InventoryManager.exe"
#define TimeManagerEXE "TimeManager.exe"
#define WorkerAdminExe "WorkerAdmin.exe"


#ifndef ApplicationVersion
  #define ApplicationVersion="21.1.2-RC2"
#endif
#ifndef Version21_0
  #define Version21_0 "21.0.9690"
#endif
#ifndef AccountManagerVersion
  #define AccountManagerVersion ApplicationVersion
#endif
#ifndef AircraftManagerVersion
  #define AircraftManagerVersion ApplicationVersion
#endif
#ifndef ADGetVersion
  #define ADGetVersion Version21_0
#endif
#ifndef ADTrackerVersion
  #define ADTrackerVersion Version21_0
#endif
#ifndef AirKeeperVersion
  #define AirKeeperVersion ApplicationVersion
#endif
#ifndef BaseManagerVersion
  #define BaseManagerVersion ApplicationVersion
#endif
#ifndef ComponentWorkReportVersion
  #define ComponentWorkReportVersion ApplicationVersion
#endif
#ifndef ElectronicSignatureVersion
  #define ElectronicSignatureVersion ApplicationVersion
#endif
#ifndef InventoryManagerVersion
  #define InventoryManagerVersion ApplicationVersion
#endif
#ifndef TimeManagerVersion
  #define TimeManagerVersion ApplicationVersion
#endif
#ifndef WorkerAdminVersion
  #define WorkerAdminVersion ApplicationVersion
#endif
#ifndef LiquibaseVersion
  #define LiquibaseVersion ApplicationVersion
#endif

;#define AMSPassword ""
#define AMSPassword "41rcr4f7m5"
#define REPOMVN "T:\mvn-repo"

#define REPO21_0 "T:\AMS_Product_Staging_Latest"

#ifndef Delivery
  #define Delivery="C:\Product\21.1.1"
#endif  
;#define DisableUninstallWelcomePage
;#ISCC.exe Example1.iss /DApplicationVersion=1.2.3.4

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{705F7A41-8FF9-49A0-815D-4050FD9C80FB}
AppName={#MyAppName}
AppVersion={#ApplicationVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
DisableDirPage=no
OutputBaseFilename=AMS_{#ApplicationVersion}
UninstallDisplayName =AMS
Compression=lzma
SolidCompression=yes

;UserInfoPage=yes
;SetupIconFile=MyProgSetup.ico
;InfoAfterFile=
WizardImageStretch=no
WizardImageFile=AMS\logo_large.bmp
WizardSmallImageFile=AMS\logo_small.bmp 
PrivilegesRequired=admin
DisableWelcomePage=no
#include "uninshs\uninshs.iss"
#include "AMS\AMSUtils.iss"
[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"; LicenseFile :"AMS\Licence\eng\LICENCE.txt";
Name: "french"; MessagesFile: compiler:Languages\French.isl; LicenseFile :"AMS\Licence\eng\LICENCE.txt"

[CustomMessages]
;FRENCH
french.InstallationTypePageTitle=Choix du type d'Installation
french.InstallationTypePageText=Veuillez selectionner le type d'installation que vous souhaiter faire. ( attention L'installtion d'une Nouvelle Workstation est proteg� par un password)
french.InstallationTypePageLabel=Type d'installation :
french.PasswordPageTitle=Mot de passe
french.PasswordPageText=Cette installation est prot�g�e par un mot de passe.
french.PasswordPageCaption=Veuillez saisir le mot de passe (attention � la distinction entre majuscules et minuscules) puis cliquez sur Suivant pour continuer.
french.PasswordPagePasswordLabel=Mot de passe :
french.CurrencyPageTitle=Choix des monnaies
french.CurrencyPageText=Veuillez selectionner les monnaies � installer
french.CurrencyPageLabelSource=Monnaies :
french.WorkstationNew=Installation d'une nouvelle Workstation
french.WorkstationUpdate=Mise a jour d'une Workstation 
french.ServerNew=Installation d'un nouveau Server
french.Server=Mise � jour du Serveur de base de donn�es
french.All=All (mise � jour seulement)
french.InventoryRBNPageTitle=
french.InventoryRBNPageText=
french.InventoryRBNPageCaption=
french.CurrencyPageAddButton=>>
french.CurrencyPageRemoveButton=<<
french.TaxesPageTitle=
french.TaxesPageText=
french.TaxesNameLabel=Nom
french.TaxesNumberLabel=Nombre
french.TaxesRateLabel=Taux
french.TaxesEmbLabel=Emb.
french.updateDataBase=Mise a jour de la base de donn�es...
french.ErrorLiquibase=Erreur pendant la mise � jour de la base de donn�es, Merci de contacter le support AMS.

;ENGLISH
english.InstallationTypePageTitle=Installation Type Selection
english.InstallationTypePageText=Please choose the type of installation you would like to do. New Workstation installation is password protected.
english.InstallationTypePageLabel=Installation Type :
english.PasswordPageTitle=Password
english.PasswordPageText=This installation is password protected.
english.PasswordPageCaption=Please provide the password, then click Next to continue. Passwords are case-sensitive.
english.PasswordPagePasswordLabel=Password :
english.CurrencyPageTitle=Currencies Selection
english.CurrencyPageText=Please select the currencies you want to install
english.CurrencyPageLabelSource=Currencies :
english.WorkstationNew=Installation of a new Workstation
english.WorkstationUpdate=Workstation update
english.ServerNew=Installation of a new Server
english.Server=DataBase server update
english.All=All (update only)
english.InventoryRBNPageTitle=
english.InventoryRBNPageText=
english.InventoryRBNPageCaption=Inventory Reports' Begininning Numbers
english.CurrencyPageAddButton=>>
english.CurrencyPageRemoveButton=<<
english.TaxesPageTitle=
english.TaxesPageText=
english.TaxesNameLabel=Name
english.TaxesNumberLabel=Number
english.TaxesRateLabel=Rate
english.TaxesEmbLabel=Emb.
english.updateDataBase=Updating databases...
english.ErrorLiquibase=An error occured while updating the database, please contact AMS Support

[Types]
Name: "full"; Description: "Full installation"
Name: "compact"; Description: "Compact installation"
Name: "custom"; Description: "Custom installation"; Flags: iscustom

[Components]
Name: account_manager; Description: "AccountManager ({#AccountManagerVersion})"; Types: full compact; Flags: DisableNoUninstallWarning;
Name: aircraft_manager; Description: "AircraftManager ({#AircraftManagerVersion})"; Types: full compact; Flags: DisableNoUninstallWarning;
Name: aircraft_manager\ad_get; Description: "ADGet ({#ADGetVersion})"; Types: full; Flags: dontinheritcheck DisableNoUninstallWarning;
Name: aircraft_manager\ad_tracker; Description: "ADTracker ({#ADTrackerVersion})"; Types: full; Flags: dontinheritcheck DisableNoUninstallWarning;
Name: air_keeper; Description: "AirKeeper ({#AirKeeperVersion})"; Types: full; Flags: DisableNoUninstallWarning;
Name: base_manager; Description: "BasesManager ({#BaseManagerVersion})"; Types: full; Flags: DisableNoUninstallWarning;
Name: component_workreport; Description: "ComponentWorkReport ({#ComponentWorkReportVersion})"; Types: full; Flags: DisableNoUninstallWarning;
Name: electronique_signature; Description: "ElectronicSignature ({#ElectronicSignatureVersion})"; Types: full; Flags: DisableNoUninstallWarning;
Name: inventory_manager; Description: "InventoryManager ({#InventoryManagerVersion})"; Types: full; Flags: DisableNoUninstallWarning;
Name: time_manager; Description: "TimeManager ({#TimeManagerVersion})"; Types: full; Flags: DisableNoUninstallWarning;
Name: worker_admin; Description: "WorkerAdmin ({#WorkerAdminVersion})"; Types: full; Flags: DisableNoUninstallWarning;

[Dirs]
Name: {app}; Permissions: everyone-modify; Check: needWorkstation;
Name: {app}\AccountManager; Permissions: everyone-modify; Components: account_manager; Check: needWorkstation;
Name: {app}\AircraftManager; Permissions: everyone-modify; Components: aircraft_manager; Check: needWorkstation;
Name: {app}\AirKeeper; Permissions: everyone-modify; Components: air_keeper; Check: needWorkstation;
Name: {app}\BasesManager; Permissions: everyone-modify; Components: base_manager; Check: needWorkstation;
Name: {app}\ComponentWorkReport; Permissions: everyone-modify; Components: component_workreport; Check: needWorkstation;
Name: {app}\ElectronicSignature; Permissions: everyone-modify; Components: electronique_signature; Check: needWorkstation;
Name: {app}\InventoryManager; Permissions: everyone-modify; Components: inventory_manager; Check: needWorkstation;
Name: {app}\TimeManager; Permissions: everyone-modify; Components: time_manager; Check: needWorkstation;
Name: {app}\WorkerAdmin; Permissions: everyone-modify; Components: worker_admin; Check: needWorkstation;
Name: {app}\Reports; Permissions: everyone-modify; Components: worker_admin; Check: needWorkstation;

[Files]
Source: "{#Delivery}\AccountManager\*"; DestDir: "{app}\AccountManager"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: account_manager; Check: needWorkstation;
Source: "{#Delivery}\AircraftManager\*"; DestDir: "{app}\AircraftManager"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#ADGetVersion}\AircraftManager\{#ADGetExe}"; DestDir: "{app}\AircraftManager"; Flags: ignoreversion; Components: aircraft_manager\ad_get; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#ADTrackerVersion}\AircraftManager\{#ADTrackerExe}"; DestDir: "{app}\AircraftManager"; Flags: ignoreversion; Components: aircraft_manager\ad_tracker; Check: needWorkstation;
Source: "{#Delivery}\AirKeeper\*"; DestDir: "{app}\AirKeeper"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: air_keeper; Check: needWorkstation;
Source: "{#Delivery}\BasesManager\*"; DestDir: "{app}\BasesManager"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: base_manager; Check: needWorkstation;
Source: "{#Delivery}\ComponentWorkReport\*"; DestDir: "{app}\ComponentWorkReport"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: component_workreport; Check: needWorkstation;
Source: "{#Delivery}\ElectronicSignature\*"; DestDir: "{app}\ElectronicSignature"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: electronique_signature; Check: needWorkstation;
Source: "{#Delivery}\InventoryManager\*"; DestDir: "{app}\InventoryManager"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: inventory_manager; Check: needWorkstation;
Source: "{#Delivery}\TimeManager\*"; DestDir: "{app}\TimeManager"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: time_manager; Check: needWorkstation;
Source: "{#Delivery}\WorkerAdmin\*"; DestDir: "{app}\WorkerAdmin"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: worker_admin; Check: needWorkstation; 


[Registry]
Root: HKLM; Subkey: "SOFTWARE\AccountManager"; ValueType: string; ValueName: "RemoteHost"; ValueData: "127.0.0.1:3307"; Flags: uninsdeletekey; Permissions: everyone-full; Components: account_manager; Check: isNewWorkstation;

Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: string; ValueName: "RemoteHost"; ValueData: "127.0.0.1:3307"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: binary; ValueName: "DueSheet"; ValueData: "00 00 00 00 c0 95 e4 40"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: dword; ValueName: "DisplayDueSheet"; ValueData: "00000000"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: dword; ValueName: "kindOfList"; ValueData: "00000001"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: string; ValueName: "TemplateWR"; ValueData: "workreport.doc"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: dword; ValueName: "SortBy"; ValueData: "00000000"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: dword; ValueName: "SortOrder"; ValueData: "00000000"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: dword; ValueName: "UseColors"; ValueData: "00000001"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: dword; ValueName: "TYPEVALUE"; ValueData: "00000000"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: dword; ValueName: "GeneralAircraft"; ValueData: "00000001"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: dword; ValueName: "checknewads"; ValueData: "00000000"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Aircraft manager 3"; ValueType: string; ValueName: "IPServerAuto"; ValueData: ""; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager; Check: isNewWorkstation;

Root: HKLM; Subkey: "SOFTWARE\Inventory Manager"; ValueType: string; ValueName: "RemoteHost"; ValueData: "127.0.0.1:3307"; Flags: uninsdeletekey; Permissions: everyone-full; Components: inventory_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Inventory Manager"; ValueType: dword; ValueName: "optionSelPart"; ValueData: "00000001"; Flags: uninsdeletekey; Permissions: everyone-full; Components: inventory_manager; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Inventory Manager"; ValueType: dword; ValueName: "PrintInfoLabel"; ValueData: "00000000"; Flags: uninsdeletekey; Permissions: everyone-full; Components: inventory_manager; Check: isNewWorkstation;

Root: HKLM; Subkey: "SOFTWARE\Component Work Report"; Flags: uninsdeletekey; Permissions: everyone-full; Components: component_workreport; Check: isNewWorkstation;

[INI]
Filename: "{app}\AMS.ini"; Section: "ReportSettings"; Key: "Dir"; String: "{app}\Reports"

[InstallDelete]
Type: filesandordirs; Name: "{app}\AccountManager"
Type: filesandordirs; Name: "{app}\AircraftManager"
Type: filesandordirs; Name: "{app}\AirKeeper"
Type: filesandordirs; Name: "{app}\BasesManager"
Type: filesandordirs; Name: "{app}\ComponentWorkReport"
Type: filesandordirs; Name: "{app}\ElectronicSignature"
Type: filesandordirs; Name: "{app}\InventoryManager"
Type: filesandordirs; Name: "{app}\TimeManager"
Type: filesandordirs; Name: "{app}\WorkerAdmin"

[UninstallDelete]
Type: filesandordirs; Name: "{app}\AccountManager"
Type: filesandordirs; Name: "{app}\AircraftManager"
Type: filesandordirs; Name: "{app}\AirKeeper"
Type: filesandordirs; Name: "{app}\BasesManager"
Type: filesandordirs; Name: "{app}\ComponentWorkReport"
Type: filesandordirs; Name: "{app}\ElectronicSignature"
Type: filesandordirs; Name: "{app}\InventoryManager"
Type: filesandordirs; Name: "{app}\TimeManager"
Type: filesandordirs; Name: "{app}\WorkerAdmin"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; Check: needWorkstation;
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; Check: needWorkstation;

[Icons]
Name: "{commonprograms}\{#MyAppName}\AccountManager"; Filename: "{app}\AccountManager\{#AccountManagerExe}"; Components : account_manager; Tasks: quicklaunchicon
Name: "{commonprograms}\{#MyAppName}\AircraftManager"; Filename: "{app}\AircraftManager\{#AmmExe}"; Components : aircraft_manager; Tasks: quicklaunchicon
Name: "{commonprograms}\{#MyAppName}\ADGet"; Filename: "{app}\AircraftManager\{#ADGetExe}"; Components : aircraft_manager\ad_get; Tasks: quicklaunchicon
Name: "{commonprograms}\{#MyAppName}\ADTracker"; Filename: "{app}\AircraftManager\{#ADTrackerExe}"; Components : aircraft_manager\ad_tracker; Tasks: quicklaunchicon
Name: "{commonprograms}\{#MyAppName}\AirKeeper"; Filename: "{app}\AirKeeper\{#AirKeeperExe}"; Components : air_keeper; Tasks: quicklaunchicon
Name: "{commonprograms}\{#MyAppName}\BasesManager"; Filename: "{app}\BasesManager\{#BaseManageExe}"; Components : base_manager; Tasks: quicklaunchicon
Name: "{commonprograms}\{#MyAppName}\ComponentWorkReport"; Filename: "{app}\ComponentWorkReport\{#CWRExe}"; Components : component_workreport; Tasks: quicklaunchicon
Name: "{commonprograms}\{#MyAppName}\ElectronicSignature"; Filename: "{app}\ElectronicSignature\{#ESExe}"; Components : electronique_signature; Tasks: quicklaunchicon
Name: "{commonprograms}\{#MyAppName}\InventoryManager"; Filename: "{app}\InventoryManager\{#InvManagerEXE}"; Components : inventory_manager; Tasks: quicklaunchicon
Name: "{commonprograms}\{#MyAppName}\TimeManager"; Filename: "{app}\TimeManager\{#TimeManagerEXE}"; Components : time_manager; Tasks: quicklaunchicon
Name: "{commonprograms}\{#MyAppName}\WorkerAdmin"; Filename: "{app}\WorkerAdmin\{#WorkerAdminExe}"; Components : worker_admin; Tasks: quicklaunchicon

Name: "{commondesktop}\{#MyAppName}\AccountManager"; Filename: "{app}\AccountManager\{#AccountManagerExe}"; Components : account_manager; Tasks: desktopicon
Name: "{commondesktop}\{#MyAppName}\AircraftManager"; Filename: "{app}\AircraftManager\{#AmmExe}"; Components : aircraft_manager; Tasks: desktopicon
Name: "{commondesktop}\{#MyAppName}\ADGet"; Filename: "{app}\AircraftManager\{#ADGetExe}"; Components : aircraft_manager\ad_get; Tasks: desktopicon
Name: "{commondesktop}\{#MyAppName}\ADTracker"; Filename: "{app}\AircraftManager\{#ADTrackerExe}"; Components : aircraft_manager\ad_tracker; Tasks: desktopicon
Name: "{commondesktop}\{#MyAppName}\AirKeeper"; Filename: "{app}\AirKeeper\{#AirKeeperExe}"; Components : air_keeper; Tasks: desktopicon
Name: "{commondesktop}\{#MyAppName}\BasesManager"; Filename: "{app}\BasesManager\{#BaseManageExe}"; Components : base_manager; Tasks: desktopicon
Name: "{commondesktop}\{#MyAppName}\ComponentWorkReport"; Filename: "{app}\ComponentWorkReport\{#CWRExe}"; Components : component_workreport; Tasks: desktopicon
Name: "{commondesktop}\{#MyAppName}\ElectronicSignature"; Filename: "{app}\ElectronicSignature\{#ESExe}"; Components : electronique_signature; Tasks: desktopicon
Name: "{commondesktop}\{#MyAppName}\InventoryManager"; Filename: "{app}\InventoryManager\{#InvManagerEXE}"; Components : inventory_manager; Tasks: desktopicon
Name: "{commondesktop}\{#MyAppName}\TimeManager"; Filename: "{app}\TimeManager\{#TimeManagerEXE}"; Components : time_manager; Tasks: desktopicon
Name: "{commondesktop}\{#MyAppName}\WorkerAdmin"; Filename: "{app}\WorkerAdmin\{#WorkerAdminExe}"; Components : worker_admin; Tasks: desktopicon


[Code]
var
  // Wizards
  InstallationTypePage : TWizardPage;
  CurrencyPage : TWizardPage;
  PasswordPage: TInputQueryWizardPage;
  InventoryRBNPage: TInputQueryWizardPage;
  TaxesPage : TWizardPage;
  CurrencyPageListBoxSource : TNewListBox;
  CurrencyPageListBoxTarget : TNewListBox;
  //Global Var
  InstallationType : String;
  administrator : Boolean;
  CurrencyList : TKeyValueList;
  //Status Boolean
  Abort : Boolean;


function needWorkstation(): Boolean;
begin
  if (InstallationType = ExpandConstant('{cm:All}')) or (InstallationType = ExpandConstant('{cm:WorkstationUpdate}')) or (InstallationType = ExpandConstant('{cm:WorkstationNew}')) then
  begin
    Result:=True;
  end else 
  begin
    Result:=False;
  end
end;

function isNewWorkstation(): Boolean;
begin
  if InstallationType = ExpandConstant('{cm:WorkstationNew}') then
  begin
    Result := True;
  end else
  begin
    Result := False;
  end
end;     

function isNewServer(): Boolean;
begin
  if InstallationType = ExpandConstant('{cm:ServerNew}') then
  begin
    Result := True;
  end else
  begin
    Result := False;
  end
end;

function needPassword() : Boolean;
begin
  Result := isNewWorkstation() or isNewServer();
end;

function isAdministrator() : Boolean;
begin
  Result := administrator;
end;

#include "AMS\Liquibase.iss"
#include "AMS\AMSBase.iss"
#include "AMS\Template.iss"

procedure InstallationTypeComboBoxChanged(Sender: TObject);
begin 
    if Sender is TNewComboBox then
    begin
      InstallationType := TNewComboBox(Sender).Text;
    end; 
end;

procedure CurrencyPageAddButtonClicked(Sender: TObject);
begin
    if ((CurrencyPageListBoxSource.ItemIndex >=0) and (CurrencyPageListBoxSource.ItemIndex < GetArrayLength(CurrencyList)) and (CurrencyPageListBoxTarget.Items.IndexOf(CurrencyList[CurrencyPageListBoxSource.ItemIndex].Key) < 0)) then
      CurrencyPageListBoxTarget.Items.Add(CurrencyList[CurrencyPageListBoxSource.ItemIndex].Key); 
end;

procedure CurrencyPageRemoveButtonClicked(Sender: TObject);
begin 
     CurrencyPageListBoxTarget.Items.Delete(CurrencyPageListBoxTarget.ItemIndex);
end;

procedure CreateInstallationTypePage;
var
  InstallationTypeLabel : TLabel;
  InstallationTypeComboBox: TNewComboBox;
begin 
  InstallationTypePage := CreateCustomPage(wpLicense, ExpandConstant('{cm:InstallationTypePageTitle}'), ExpandConstant('{cm:InstallationTypePageText}'));

  InstallationTypeLabel := TLabel.Create(InstallationTypePage);
  InstallationTypeLabel.Caption := ExpandConstant('{cm:InstallationTypePageLabel}');
  InstallationTypeLabel.Parent := InstallationTypePage.Surface;

  InstallationTypeComboBox := TNewComboBox.Create(InstallationTypePage);
  InstallationTypeComboBox.Top := InstallationTypeLabel.Top + InstallationTypeLabel.Height + ScaleY(4);
  InstallationTypeComboBox.Width := InstallationTypePage.SurfaceWidth - ScaleX(51)
  InstallationTypeComboBox.OnChange := @InstallationTypeComboBoxChanged;
  InstallationTypeComboBox.Parent := InstallationTypePage.Surface;
  
  InstallationTypeComboBox.Items.Add(ExpandConstant('{cm:All}'));
  InstallationTypeComboBox.Items.Add(ExpandConstant('{cm:WorkstationUpdate}'));
  InstallationTypeComboBox.Items.Add(ExpandConstant('{cm:Server}'));
  //InstallationTypeComboBox.Items.Add(ExpandConstant('{cm:ServerNew}'));
  InstallationTypeComboBox.Items.Add(ExpandConstant('{cm:WorkstationNew}'));
  
  InstallationTypeComboBox.ItemIndex := 0;
  InstallationType := ExpandConstant('{cm:All}');
end;

procedure CreatePasswordPage;
begin 
  PasswordPage := CreateInputQueryPage(InstallationTypePage.ID, ExpandConstant('{cm:PasswordPageTitle}'), ExpandConstant('{cm:PasswordPageText}'), ExpandConstant('{cm:PasswordPageCaption}')); 
  PasswordPage.Add(SetupMessage(msgPasswordEditLabel), True);
end;

procedure CreateInventoryRBNPage;
begin 
  InventoryRBNPage := CreateInputQueryPage(TaxesPage.ID, ExpandConstant('{cm:InventoryRBNPageTitle}'), ExpandConstant('{cm:InventoryRBNPageText}'), ExpandConstant('{cm:InventoryRBNPageCaption}'));
  InventoryRBNPage.Add('Invoice #:', False);
  InventoryRBNPage.Add('Op. Cost #:', False);
  InventoryRBNPage.Add('P.O. #:', False);
  InventoryRBNPage.Add('Quotation #:', False);
  InventoryRBNPage.Add('Estimation #:', False);
end;

procedure CreateCurrencyPage;
var
  I:Integer;
  CurrencyPageLabelSource : TLabel;
  CurrencyPageAddButton : TNewButton;
  CurrencyPageRemoveButton : TNewButton;
begin 
  CurrencyPage :=  CreateCustomPage(EditBasePage2.ID, ExpandConstant('{cm:CurrencyPageTitle}'), ExpandConstant('{cm:CurrencyPageText}'));

  CurrencyPageLabelSource := TLabel.Create(CurrencyPage);
  CurrencyPageLabelSource.Caption := ExpandConstant('{cm:CurrencyPageLabelSource}');
  CurrencyPageLabelSource.Parent := CurrencyPage.Surface;

  CurrencyPageListBoxSource := TNewListBox.Create(CurrencyPage);
  CurrencyPageListBoxSource.Top := CurrencyPageLabelSource.Top + CurrencyPageLabelSource.Height + ScaleY(4);
  CurrencyPageListBoxSource.Width := CurrencyPage.SurfaceWidth - ScaleX(250)
  CurrencyPageListBoxSource.Height := CurrencyPage.SurfaceHeight - ScaleY(20)
  CurrencyPageListBoxSource.OnDblClick := @CurrencyPageAddButtonClicked;
  CurrencyPageListBoxSource.Parent := CurrencyPage.Surface;
  
  CurrencyPageAddButton := TNewButton.create(CurrencyPage);
  CurrencyPageAddButton.Caption:= ExpandConstant('{cm:CurrencyPageAddButton}');
  CurrencyPageAddButton.Top := CurrencyPageListBoxSource.Top + CurrencyPageLabelSource.Height + CurrencyPageListBoxSource.Height/3;
  CurrencyPageAddButton.Left := CurrencyPageListBoxSource.Left + CurrencyPageListBoxSource.Width + ScaleX(20);
  CurrencyPageAddButton.OnClick := @CurrencyPageAddButtonClicked;
  CurrencyPageAddButton.Parent := CurrencyPage.Surface;

  CurrencyPageRemoveButton := TNewButton.create(CurrencyPage);
  CurrencyPageRemoveButton.Caption:= ExpandConstant('{cm:CurrencyPageRemoveButton}');
  CurrencyPageRemoveButton.Top := CurrencyPageAddButton.Top + ScaleY(20)
  CurrencyPageRemoveButton.Left := CurrencyPageAddButton.Left;
  CurrencyPageRemoveButton.OnClick := @CurrencyPageRemoveButtonClicked;
  CurrencyPageRemoveButton.Parent := CurrencyPage.Surface;

  CurrencyPageListBoxTarget := TNewListBox.Create(CurrencyPage);
  CurrencyPageListBoxTarget.Top := CurrencyPageLabelSource.Top + CurrencyPageLabelSource.Height + ScaleY(4);
  CurrencyPageListBoxTarget.Width := CurrencyPage.SurfaceWidth - ScaleX(250);
  CurrencyPageListBoxTarget.Height := CurrencyPage.SurfaceHeight - ScaleY(20);
  CurrencyPageListBoxTarget.Left := CurrencyPageListBoxSource.Left + CurrencyPageListBoxSource.Width + ScaleX(80);
  CurrencyPageListBoxTarget.OnDblClick := @CurrencyPageRemoveButtonClicked;
  CurrencyPageListBoxTarget.Parent := CurrencyPage.Surface;

  CurrencyList := getCurrencyList();
  for I:= 0 to GetArrayLength(CurrencyList)-1 do
  begin
    CurrencyPageListBoxSource.Items.Add(CurrencyList[I].Key);
  end;
  // Preinstall EUR, USD and CAD
  for I:= 0 to GetArrayLength(CurrencyList)-1 do
  begin
      if (CurrencyList[I].Key = 'EUR') or (CurrencyList[I].Key = 'USD') or (CurrencyList[I].Key = 'CAD') then
      CurrencyPageListBoxTarget.Items.Add(CurrencyList[I].Key);
  end;
end;

procedure CreateTaxesPage;
var
  TaxesNameLabel : TLabel;
  TaxesNameEdit : TNewEdit;
  TaxesNumberLabel : TLabel;
  TaxesNumberEdit : TNewEdit;
  TaxesRateLabel : TLabel;
  TaxesRateEdit : TNewEdit;
  TaxesRateEdit : TNewEdit;
  TaxesEmbLabel : TLabel;
  TaxesEmdCheckBox : TNewCheckBox;
begin
  TaxesPage :=  CreateCustomPage(CurrencyPage.ID, ExpandConstant('{cm:TaxesPageTitle}'), ExpandConstant('{cm:TaxesPageText}'));

  TaxesNameLabel := TLabel.Create(TaxesPage);
  TaxesNameLabel.Caption := ExpandConstant('{cm:TaxesNameLabel}');
  TaxesNameLabel.Parent := TaxesPage.Surface;

  TaxesNumberLabel  := TLabel.Create(TaxesPage);
  TaxesNumberLabel.Caption := ExpandConstant('{cm:TaxesNumberLabel}');
  TaxesNumberLabel.Parent := TaxesPage.Surface;
  
  TaxesRateLabel  := TLabel.Create(TaxesPage);
  TaxesRateLabel.Caption := ExpandConstant('{cm:TaxesRateLabel}');
  TaxesRateLabel.Parent := TaxesPage.Surface;
   
  TaxesEmbLabel  := TLabel.Create(TaxesPage);
  TaxesEmbLabel.Caption := ExpandConstant('{cm:TaxesEmbLabel}');
  TaxesEmbLabel.Parent := TaxesPage.Surface;

  TaxesNameEdit := TNewEdit.Create(TaxesPage);
  //TaxesNameEdit.Top := LogoLabel.Top;
  //TaxesNameEdit.Left :=  LogoLabel.Left + ScaleX(80);
  //TaxesNameEdit.Width := ScaleX(200)
  TaxesNameEdit.Parent := TaxesPage.Surface;

  TaxesNumberEdit := TNewEdit.Create(TaxesPage);
  //TaxesNumberEdit.Top := LogoLabel.Top;
  //TaxesNumberEdit.Left :=  LogoLabel.Left + ScaleX(80);
  //TaxesNumberEdit.Width := ScaleX(200)
  TaxesNumberEdit.Parent := TaxesPage.Surface;

  TaxesRateEdit := TNewEdit.Create(TaxesPage);
  //TaxesRateEdit.Top := LogoLabel.Top;
  //TaxesRateEdit.Left :=  LogoLabel.Left + ScaleX(80);
  //TaxesRateEdit.Width := ScaleX(200)
  TaxesRateEdit.Parent := TaxesPage.Surface;
  
  TaxesEmdCheckBox := TNewCheckBox.Create(TaxesPage);
  //TaxesEmdCheckBox.Top := PrimaryLabel.Top;
  //TaxesEmdCheckBox.Height := PrimaryLabel.Height; 
  //TaxesEmdCheckBox.Left := BaseNameEdit.Left;  
  TaxesEmdCheckBox.Parent := TaxesPage.Surface;
end;

procedure InitializeWizard;
begin
  administrator := False;
  UninsHs_InitializeWizard();
  CreateInstallationTypePage;
  CreatePasswordPage;
  CreateBasePage();
  CreateEditBasePage1();
  CreateEditBasePage2();
  CreateCurrencyPage();
  CreateTaxesPage();
  CreateInventoryRBNPage();
  CreateDataBaseServerPage();
  CreateTemplateFormatPage();
end; 

procedure CurPageChanged(CurPageID: Integer);
begin
  UninsHs_CurPageChanged(CurPageID);
  if (CurPageId = BasePage.ID) then
      begin
        if  (GetArrayLength(BaseList) =  0 ) then
          Wizardform.NextButton.Enabled := False;
      end
  else if (CurPageId = EditBasePage2.ID) then
    Wizardform.NextButton.Enabled := False
  else if (CurPageId = wpInstalling) then
    begin
	
    end;
end;

function ShouldSkipPage(CurPageId: Integer): Boolean;
begin
  Result := False;
  UninsHs_ShouldSkipPage(CurPageId, Result);
  if (CurPageID = wpSelectComponents) and not needWorkstation() then
    Result := True
  else if (CurPageID = wpReady) and not needWorkstation() then
    Result := True
  else if (CurPageID = PasswordPage.ID) and not needPassword() then
    Result := True
  else if (CurPageID = PasswordPage.ID) and administrator then
    Result := True
  else if (CurPageId = InventoryRBNPage.ID) and not isNewServer() then
    Result := True
  else if (CurPageId = CurrencyPage.ID) and not isNewServer() then
    Result := True
  else if (CurPageId = BasePage.ID) and not isNewServer() then
    Result := True
  else if (CurPageId = EditBasePage1.ID)  and  not isNewServer() then
      Result := True
  else if (CurPageId = EditBasePage1.ID)  and not (isBaseCreate or isBaseEdit) then
      Result := True
  else if (CurPageId = EditBasePage2.ID) and not isNewServer() then
      Result := True
  else if (CurPageId = EditBasePage2.ID)  and not (isBaseCreate or isBaseEdit) then
      Result := True
  else if (CurPageId = TaxesPage.ID)  and  not isNewServer() then
      Result := True
  else if (CurPageId = wpSelectDir) and not needWorkstation() then
    Result := True
  else if (CurPageId = DataBaseServerPage.ID) and not needLiquibase() then
    Result := True
  else if (CurPageId = TemplateFormatPage.ID) and not needWorkstation() then
    Result := True
  else
    Result := Abort;
end;

function NextButtonClick(CurPageID: Integer): Boolean;
begin
  Result := True;
  UninsHs_NextButtonClick(CurPageID, Result);
  if (CurPageID = PasswordPage.ID) then begin
    if PasswordPage.Edits[0].Text <> '{#AMSPassword}' then begin
      MsgBox(SetupMessage(msgIncorrectPassword), mbError, MB_OK);
      administrator := False;
      Result := False;
    end else
        administrator := True;
    end;
end;

procedure CancelButtonClick(CurPageID: Integer; var Cancel, Confirm: Boolean);
begin
  UninsHs_CancelButtonClick(CurPageID, Cancel, Confirm);
end;

function InitializeUninstall(): Boolean;
begin
  Result := True;
  UninsHs_InitializeUninstall(Result);
end;