param(
    [string] $Path = "C:\Program Files (x86)\AMS",
	[string ] $Format = "A4"
)

$word = New-Object -com Word.Application


$docFiles = (Get-ChildItem $Path\$templateFolder -Include *.docx, *.doc , *.rtf -Recurse)

foreach ($docFile in $docFiles) {
	write-output $docFile.FullName
	$doc = $word.Documents.Open($docFile.FullName)
	$layout = $doc.PageSetup.Orientation
	
	If ($Format -eq "10x14"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaper10x14
	}
	elseIf ($Format -eq "11x17"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaper11x17
	}
	elseIf ($Format -eq "Letter"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperLetter
	}
	elseIf ($Format -eq "LetterSmall"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperLetterSmall
	}
	elseIf ($Format -eq "Legal"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperLegal
	}
	elseIf ($Format -eq "Executive"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperExecutive
	}
	elseIf ($Format -eq "A4") {
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperA4
	}
	elseIf ($Format -eq "A3"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperA3
	}
	elseIf ($Format -eq "Legal"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperLegal
	}
	elseIf ($Format -eq "A4Small"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperA4Small
	}
	elseIf ($Format -eq "A5"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperA5
	}
	elseIf ($Format -eq "B4"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperB4
	}
	elseIf ($Format -eq "B5"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperB5
	}
	elseIf ($Format -eq "CSheet"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperCSheet
	}
	elseIf ($Format -eq "DSheet"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperDSheet
	}
	elseIf ($Format -eq "ESheet"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperESheet
	}
	elseIf ($Format -eq "FanfoldLegalGerman"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperFanfoldLegalGerman
	}
	elseIf ($Format -eq "FanfoldStdGerman"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperFanfoldStdGerman
	}
	elseIf ($Format -eq "FanfoldUS"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperFanfoldUS
	}
	elseIf ($Format -eq "Folio"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperFolio
	}
	elseIf ($Format -eq "Ledger"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperLedger
	}
	elseIf ($Format -eq "Note"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperNote
	}
	elseIf ($Format -eq "Quarto"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperQuarto
	}
	elseIf ($Format -eq "Statement"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperStatement
	}
	elseIf ($Format -eq "Tabloid"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperTabloid
	}
	elseIf ($Format -eq "Envelope9"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelope9
	}
	elseIf ($Format -eq "Envelope10"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelope10
	}
	elseIf ($Format -eq "Envelope11"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelope11
	}
	elseIf ($Format -eq "Envelope12"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelope12
	}
	elseIf ($Format -eq "Envelope14"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelope14
	}
	elseIf ($Format -eq "EnvelopeB4"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopeB4
	}
	elseIf ($Format -eq "EnvelopeB5"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopeB5
	}
	elseIf ($Format -eq "EnvelopeB6"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopeB6
	}
	elseIf ($Format -eq "EnvelopeC3"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopeC3
	}
	elseIf ($Format -eq "EnvelopeC4"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopeC4
	}
	elseIf ($Format -eq "EnvelopeC5"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopeC5
	}
	elseIf ($Format -eq "EnvelopeC6"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopeC6
	}
	elseIf ($Format -eq "EnvelopeC65"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopeC65
	}
	elseIf ($Format -eq "EnvelopeDL"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopeDL
	}
	elseIf ($Format -eq "EnvelopeItaly"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopeItaly
	}
	elseIf ($Format -eq "EnvelopeMonarch"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopeMonarch
	}
	elseIf ($Format -eq "EnvelopePersonal"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperEnvelopePersonal
	}
	elseIf ($Format -eq "wdPaperCustom"){
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperCustom
	}	
	else {
		$doc.PageSetup.PaperSize = [Microsoft.Office.Interop.Word.WdPaperSize]::wdPaperLetter
	}
	if ($layout -eq 1) {
		$doc.PageSetup.Orientation = [Microsoft.Office.Interop.Word.WdOrientation]::wdOrientLandscape
	}
	$doc.Save()
	$doc.Close()

}

$word.Quit()