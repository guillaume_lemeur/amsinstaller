[CustomMessages]
;FRENCH
french.configurationAMMRR=Installation de AMMRemote Reporting ...
french.configureAMMRRYesNo=Configurer AMMRemote Reporting maintenant ?
french.uninstallAMMRRRYesNo==Voulez vous supprimer AMMRemote Reporting Restarter ?
french.installAMMRRRYesNo=Installer AMMRemote Reporting Restarter ?
french.ErrorAMMRRR=Erreur pendant la suppression de AMM Remote Reporting Restarter, Merci de contacter le support AMS.
;ENGLISH
english.configurationAMMRR=Installing AMMRemote Reporting ...
english.installAMMRRRYesNo=Do you want to install AMMRemote Reporting Restarter ?
english.uninstallAMMRRRYesNo=Do you want to unistall AMMRemote Reporting Restarter ?
english.configureAMMRRYesNo=Do you want to configure AMMRemote Reporting now ?
english.ErrorAMMRRR=An error occured while uninstalling AMM Remote Reporting Restarter, please contact AMS Support.

[Dirs]
Name: {app}\AMM Remote Reporting; Permissions: everyone-modify; Components: aircraft_manager\ammrr; Check: needWorkstation;
Name: C:\windows\SysWOW64\config\systemprofile\Desktop; Permissions: everyone-modify; Components: aircraft_manager\ammrr; Check: IsWin64 and needWorkstation;
Name: C:\windows\System32\config\systemprofile\Desktop; Permissions: everyone-modify; Components: aircraft_manager\ammrr; Check: (not IsWin64) and needWorkstation;

[Files]
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\AMMRemoteReporting.exe"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\Cert\*"; DestDir: "{app}\AMM Remote Reporting\Cert"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\ComputeMaintenance.dll"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\FlagCardReport.dll"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\libeay32.dll"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\libmysql.dll"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\qryDueAD.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\qryDueComp.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\qryDueInspections.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\qryDueMeters.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\qryDueSnags.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\qryDueSpecialTask.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\qryDueTask.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\RaportWorkReport.dll"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\SOFTWARE LICENSE AGREEMENT.pdf"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\ssleay32.dll"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\tblAD.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\tblhobbinspect.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\tblinspect.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\tblinspections.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\tblTask.bin"; DestDir: "{app}\AMM Remote Reporting"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\temp\*"; DestDir: "{app}\AMM Remote Reporting\temp"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\Templates\*"; DestDir: "{app}\AMM Remote Reporting\Templates"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\WebRoot\*"; DestDir: "{app}\AMM Remote Reporting\WebRoot"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
;AMMRRRestarter
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\AMMRRRestarter\setup.exe"; DestDir: "{tmp}\AMMRRRestarter"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;
Source: "{#REPO21_0}\AMS_Products-{#AMMRRVersion}\AMMRR\AMMRRRestarter\AMMRRRSetup.msi"; DestDir: "{tmp}\AMMRRRestarter"; Flags: recursesubdirs createallsubdirs ignoreversion; Components: aircraft_manager\ammrr; Check: needWorkstation;

[Registry]
Root: HKLM; Subkey: "SOFTWARE\Microsoft\Ole"; ValueType: string; ValueName: "EnableDCOM"; ValueData: "Y"; Permissions: everyone-full; Components: aircraft_manager\ammrr; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Microsoft\Ole"; ValueType: dword; ValueName: "LegacyAuthenticationLevel"; ValueData: "1"; Permissions: everyone-full; Components: aircraft_manager\ammrr; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\Microsoft\Ole"; ValueType: dword; ValueName: "LegacyImpersonationLevel"; ValueData: "2"; Permissions: everyone-full; Components: aircraft_manager\ammrr; Check: isNewWorkstation;

Root: HKLM; Subkey: "SOFTWARE\AMMRR"; ValueType: string; ValueName: "Year"; ValueData: "b&�"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager\ammrr; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\AMMRR"; ValueType: string; ValueName: "Month"; ValueData: ""; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager\ammrr; Check: isNewWorkstation;
Root: HKLM; Subkey: "SOFTWARE\AMMRR"; ValueType: string; ValueName: "Day"; ValueData: "j"; Flags: uninsdeletekey; Permissions: everyone-full; Components: aircraft_manager\ammrr; Check: isNewWorkstation;

[Run]
;Install AMMRRRestarter
Filename: "{tmp}\AMMRRRestarter\setup.exe"; StatusMsg: ExpandConstant('{cm:configurationAMMRR}'); Check: installationAMMRRRestarter; Components: aircraft_manager\ammrr;
;Configure AMMRR
Filename: "{app}\AMM Remote Reporting\AMMRemoteReporting.exe";  Parameters: "/SETUP"; StatusMsg: ExpandConstant('{cm:configurationAMMRR}'); Check: configureAMMRR; Components: aircraft_manager\ammrr; 

[InstallDelete]
Type: filesandordirs; Name: "{app}\AMM Remote Reporting"

[UninstallDelete]
Type: filesandordirs; Name: "{app}\AMM Remote Reporting"

[code]
function installationAMMRRRestarter() : Boolean;
begin
  if needWorkstation() then
    begin
      Result := MsgBox(ExpandConstant('{cm:installAMMRRRYesNo}'), mbConfirmation, MB_YESNO) = idYes;
    end
   else
    begin
      Result := False;
    end;
end;

function uninstallAMMRRRestarter() : Boolean;
begin
  Result := MsgBox(ExpandConstant('{cm:uninstallAMMRRRYesNo}'), mbConfirmation, MB_YESNO) = idYes;
end;

function configureAMMRR() : Boolean;
begin
  if needWorkstation() then
    begin
      Result := MsgBox(ExpandConstant('{cm:configureAMMRRYesNo}'), mbConfirmation, MB_YESNO) = idYes;
    end
   else
    begin
      Result := False;
    end;
end;

function getUninsAMMRRR() : String;
var
  AMRRR_SSID : String;
begin 
   Result := '';
   AMRRR_SSID := '{B6F8E35B-BAF0-4B77-9173-04F980808E45}';
   RegQueryStringValue(HKEY_LOCAL_MACHINE, RegPathUninstall() + AMRRR_SSID, 'UninstallString', Result);
end;

function isAMMRRRInstalled() : Boolean;
begin 
  Result := (getUninsAMMRRR() <> '');
end; 

procedure runAMMRRRSetup();
var
  ResultCode : Integer;
  FileName : String;
begin
   if not runMsi('{B6F8E35B-BAF0-4B77-9173-04F980808E45}', '') then
   begin
      MsgBox(ExpandConstant('{cm:ErrorAMMRRR}'), mbCriticalError, MB_OK);
   end
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  if CurUninstallStep = usUninstall then 
  begin
    if isAMMRRRInstalled() then
      runAMMRRRSetup();
  end;
end;