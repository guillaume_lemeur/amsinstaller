[CustomMessages]
french.TemplateFormatPageTitle=Format des templates.
french.TemplateFormatPageText=Veuillez choisir le formnat de vos templates.
french.TempalteFormatLabel=Format
french.Letter=Letter
french.A4=A4
french.updateACMTemplate=Mise a jour des templates pour AccountManager...
french.updateAMMTemplate=Mise a jour des templates pour AircraftManager...
french.updateAMMRRTemplate=Mise a jour des templates pour AMM Remote Reporting...
french.updateCWRTemplate=Mise a jour des templates pour ComponentWorkReport...
french.updateESTemplate=Mise a jour des templates pour ElectronicSignature...
french.updateIMTemplate=Mise a jour des templates pour InventoryManager...
french.updateTMTemplate=Mise a jour des templates pour TimeManager...

english.TemplateFormatPageTitle=Templates format.
english.TemplateFormatPageText=Please choose your templates format.
english.TempalteFormatLabel=Format
english.Letter=Letter
english.A4=A4
english.updateACMTemplate=Updating templates for AccountManager...
english.updateAMMTemplate=Updating templates for AircraftManager...
english.updateAMMRRTemplate=Updating templates for AMM Remote Reporting...
english.updateCWRTemplate=Updating templates for ComponentWorkReport... 
english.updateESTemplate=Updating templates for ElectronicSignature...
english.updateIMTemplate=Updating templates for InventoryManager...
english.updateTMTemplate=Updating templates for TimeManager...

[Files]
Source: "templateFormatChanger\TemplateFormatChanger.ps1"; DestDir: "{tmp}"; Flags: recursesubdirs createallsubdirs; Check: needWorkstation;

[Run]
;Create Update Liquibase
Filename: "powershell.exe"; Parameters: "-executionpolicy remotesigned -File ""{tmp}\TemplateFormatChanger.ps1"" -Path ""{app}\AccountManager\Templates"" -Format {code:GetTemplateFormat}"; Flags:runhidden; StatusMsg: {cm:updateACMTemplate}; Check: needUpdateACMTemplate;
Filename: "powershell.exe"; Parameters: "-executionpolicy remotesigned -File ""{tmp}\TemplateFormatChanger.ps1"" -Path ""{app}\AircraftManager\templates"" -Format {code:GetTemplateFormat}"; Flags:runhidden; StatusMsg: {cm:updateAMMTemplate}; Check: needUpdateAMMTemplate;
Filename: "powershell.exe"; Parameters: "-executionpolicy remotesigned -File ""{tmp}\TemplateFormatChanger.ps1"" -Path ""{app}\AircraftManager\templateSpecifique"" -Format {code:GetTemplateFormat}"; Flags:runhidden; StatusMsg: {cm:updateAMMTemplate}; Check: needUpdateAMMTemplate;
Filename: "powershell.exe"; Parameters: "-executionpolicy remotesigned -File ""{tmp}\TemplateFormatChanger.ps1"" -Path ""{app}\AMM Remote Reporting\Templates"" -Format {code:GetTemplateFormat}"; Flags:runhidden; StatusMsg: {cm:updateAMMRRTemplate}; Check: needUpdateAMMRRTemplate;
Filename: "powershell.exe"; Parameters: "-executionpolicy remotesigned -File ""{tmp}\TemplateFormatChanger.ps1"" -Path ""{app}\ComponentWorkReport\templates"" -Format {code:GetTemplateFormat}"; Flags:runhidden; StatusMsg: {cm:updateCWRTemplate}; Check: needUpdateCWRTemplate;
Filename: "powershell.exe"; Parameters: "-executionpolicy remotesigned -File ""{tmp}\TemplateFormatChanger.ps1"" -Path ""{app}\ComponentWorkReport\templateJAI"" -Format {code:GetTemplateFormat}"; Flags:runhidden; StatusMsg: {cm:updateCWRTemplate}; Check: needUpdateCWRTemplate;
Filename: "powershell.exe"; Parameters: "-executionpolicy remotesigned -File ""{tmp}\TemplateFormatChanger.ps1"" -Path ""{app}\ElectronicSignature\templates"" -Format {code:GetTemplateFormat}"; Flags:runhidden; StatusMsg: {cm:updateESTemplate}; Check: needUpdateESTemplate;
Filename: "powershell.exe"; Parameters: "-executionpolicy remotesigned -File ""{tmp}\TemplateFormatChanger.ps1"" -Path ""{app}\InventoryManager\templates"" -Format {code:GetTemplateFormat}"; Flags:runhidden; StatusMsg: {cm:updateIMTemplate}; Check: needUpdateIMTemplate;
Filename: "powershell.exe"; Parameters: "-executionpolicy remotesigned -File ""{tmp}\TemplateFormatChanger.ps1"" -Path ""{app}\TimeManager\Templates"" -Format {code:GetTemplateFormat}"; Flags:runhidden; StatusMsg: {cm:updateTMTemplate}; Check: needUpdateTMTemplate;

[Code]
var
  TemplateFormatPage : TWizardPage;
  TemplateFormatType : String;

function GetTemplateFormat(Param: String) : String;
begin
  Result := TemplateFormatType;
end;

function needUpdateACMTemplate() : Boolean;
begin
  Result := needWorkstation() and IsComponentSelected('account_manager') and (TemplateFormatType = ExpandConstant('{cm:A4}'));
end;

function needUpdateAMMTemplate() : Boolean;
begin
  Result := needWorkstation() and IsComponentSelected('aircraft_manager') and (TemplateFormatType = ExpandConstant('{cm:A4}'));
end;

function needUpdateAMMRRTemplate() : Boolean;
begin
  Result := needWorkstation() and IsComponentSelected('aircraft_manager\ammrr') and (TemplateFormatType = ExpandConstant('{cm:A4}'));
end;

function needUpdateCWRTemplate() : Boolean;
begin
  Result := needWorkstation() and IsComponentSelected('component_workreport') and (TemplateFormatType = ExpandConstant('{cm:A4}'));
end;

function needUpdateESTemplate() : Boolean;
begin
  Result := needWorkstation() and IsComponentSelected('electronique_signature') and (TemplateFormatType = ExpandConstant('{cm:A4}'));
end;

function needUpdateIMTemplate() : Boolean;
begin
  Result := needWorkstation() and IsComponentSelected('inventory_manager') and (TemplateFormatType = ExpandConstant('{cm:A4}'));
end;

function needUpdateTMTemplate() : Boolean;
begin
  Result := needWorkstation() and IsComponentSelected('time_manager') and (TemplateFormatType = ExpandConstant('{cm:A4}'));
end;

procedure TemplateFormatComboBoxChanged(Sender: TObject);
begin 
    if Sender is TNewComboBox then
    begin
      TemplateFormatType := TNewComboBox(Sender).Text;
    end; 
end;

procedure CreateTemplateFormatPage();
var
  TemplateFormatLabel : TLabel;
  TemplateFormatComboBox: TNewComboBox;
begin
  TemplateFormatPage := CreateCustomPage(DataBaseServerPage.ID, ExpandConstant('{cm:TemplateFormatPageTitle}'), ExpandConstant('{cm:TemplateFormatPageText}'));

  TemplateFormatLabel := TLabel.Create(TemplateFormatPage);
  TemplateFormatLabel.Caption := ExpandConstant('{cm:TempalteFormatLabel}');
  TemplateFormatLabel.Parent := TemplateFormatPage.Surface;
  
  TemplateFormatComboBox := TNewComboBox.Create(TemplateFormatPage);
  TemplateFormatComboBox.Top := TemplateFormatLabel.Top + TemplateFormatLabel.Height + ScaleY(4);
  TemplateFormatComboBox.Width := TemplateFormatPage.SurfaceWidth - ScaleX(51)
  TemplateFormatComboBox.OnChange := @TemplateFormatComboBoxChanged;
  TemplateFormatComboBox.Parent := TemplateFormatPage.Surface;
  
  TemplateFormatComboBox.Items.Add(ExpandConstant('{cm:Letter}'));
  TemplateFormatComboBox.Items.Add(ExpandConstant('{cm:A4}'));
  
  TemplateFormatComboBox.ItemIndex := 0;
  TemplateFormatType := ExpandConstant('{cm:Letter}');

end;