
[CustomMessages]
french.AED=United Arab Emirates Dirham
french.AFN=Afghanistan Afghani
french.ALL=Albania Lek
french.AMD=Armenia Dram
french.ANG=Netherlands Antilles Guilder
french.AOA=Angola Kwanza
french.ARS=Argentina Peso
french.AUD=Australia Dollar
french.AWG=Aruba Guilder
french.AZN=Azerbaijan Manat
french.BAM=Bosnia and Herzegovina Convertible Marka
french.BBD=Barbados Dollar
french.BDT=Bangladesh Taka
french.BGN=Bulgaria Lev
french.BHD=Bahrain Dinar
french.BIF=Burundi Franc
french.BMD=Bermuda Dollar
french.BND=Brunei Darussalam Dollar
french.BOB=Bolivia Bol�viano
french.BRL=Brazil Real
french.BSD=Bahamas Dollar
french.BTN=Bhutan Ngultrum
french.BWP=Botswana Pula
french.BYN=Belarus Ruble
french.BZD=Belize Dollar
french.CAD=Canada Dollar
french.CDF=Congo/Kinshasa Franc
french.CHF=Switzerland Franc
french.CLP=Chile Peso
french.CNY=China Yuan Renminbi
french.COP=Colombia Peso
french.CRC=Costa Rica Colon
french.CUC=Cuba Convertible Peso
french.CUP=Cuba Peso
french.CVE=Cape Verde Escudo
french.CZK=Czech Republic Koruna
french.DJF=Djibouti Franc
french.DKK=Denmark Krone
french.DOP=Dominican Republic Peso
french.DZD=Algeria Dinar
french.EGP=Egypt Pound
french.ERN=Eritrea Nakfa
french.ETB=Ethiopia Birr
french.EUR=Euro Member Countries
french.FJD=Fiji Dollar
french.FKP=Falkland Islands (Malvinas) Pound
french.GBP=United Kingdom Pound
french.GEL=Georgia Lari
french.GGP=Guernsey Pound
french.GHS=Ghana Cedi
french.GIP=Gibraltar Pound
french.GMD=Gambia Dalasi
french.GNF=Guinea Franc
french.GTQ=Guatemala Quetzal
french.GYD=Guyana Dollar
french.HKD=Hong Kong Dollar
french.HNL=Honduras Lempira
french.HRK=Croatia Kuna
french.HTG=Haiti Gourde
french.HUF=Hungary Forint
french.IDR=Indonesia Rupiah
french.ILS=Israel Shekel
french.IMP=Isle of Man Pound
french.INR=India Rupee
french.IQD=Iraq Dinar
french.IRR=Iran Rial
french.ISK=Iceland Krona
french.JEP=Jersey Pound
french.JMD=Jamaica Dollar
french.JOD=Jordan Dinar
french.JPY=Japan Yen
french.KES=Kenya Shilling
french.KGS=Kyrgyzstan Som
french.KHR=Cambodia Riel
french.KMF=Comorian Franc
french.KPW=Korea (North) Won
french.KRW=Korea (South) Won
french.KWD=Kuwait Dinar
french.KYD=Cayman Islands Dollar
french.KZT=Kazakhstan Tenge
french.LAK=Laos Kip
french.LBP=Lebanon Pound
french.LKR=Sri Lanka Rupee
french.LRD=Liberia Dollar
french.LSL=Lesotho Loti
french.LYD=Libya Dinar
french.MAD=Morocco Dirham
french.MDL=Moldova Leu
french.MGA=Madagascar Ariary
french.MKD=Macedonia Denar
french.MMK=Myanmar (Burma) Kyat
french.MNT=Mongolia Tughrik
french.MOP=Macau Pataca
french.MRO=Mauritania Ouguiya
french.MUR=Mauritius Rupee
french.MVR=Maldives (Maldive Islands) Rufiyaa
french.MWK=Malawi Kwacha
french.MXN=Mexico Peso
french.MYR=Malaysia Ringgit
french.MZN=Mozambique Metical
french.NAD=Namibia Dollar
french.NGN=Nigeria Naira
french.NIO=Nicaragua Cordoba
french.NOK=Norway Krone
french.NPR=Nepal Rupee
french.NZD=New Zealand Dollar
french.OMR=Oman Rial
french.PAB=Panama Balboa
french.PEN=Peru Sol
french.PGK=Papua New Guinea Kina
french.PHP=Philippines Peso
french.PKR=Pakistan Rupee
french.PLN=Poland Zloty
french.PYG=Paraguay Guarani
french.QAR=Qatar Riyal
french.RON=Romania Leu
french.RSD=Serbia Dinar
french.RUB=Russia Ruble
french.RWF=Rwanda Franc
french.SAR=Saudi Arabia Riyal
french.SBD=Solomon Islands Dollar
french.SCR=Seychelles Rupee
french.SDG=Sudan Pound
french.SEK=Sweden Krona
french.SGD=Singapore Dollar
french.SHP=Saint Helena Pound
french.SLL=Sierra Leone Leone
french.SOS=Somalia Shilling
french.SPL=Seborga Luigino
french.SRD=Suriname Dollar
french.STD=S�o Tom� and Pr�ncipe Dobra
french.SVC=El Salvador Colon
french.SYP=Syria Pound
french.SZL=Swaziland Lilangeni
french.THB=Thailand Baht
french.TJS=Tajikistan Somoni
french.TMT=Turkmenistan Manat
french.TND=Tunisia Dinar
french.TOP=Tonga Pa'anga
french.TRY=Turkey Lira
french.TTD=Trinidad and Tobago Dollar
french.TVD=Tuvalu Dollar
french.TWD=Taiwan New Dollar
french.TZS=Tanzania Shilling
french.UAH=Ukraine Hryvnia
french.UGX=Uganda Shilling
french.USD=United States Dollar
french.UYU=Uruguay Peso
french.UZS=Uzbekistan Som
french.VEF=Venezuela Bol�var
french.VND=Viet Nam Dong
french.VUV=Vanuatu Vatu
french.WST=Samoa Tala
french.XAF=Communaut� Financi�re Africaine (BEAC) CFA Franc�BEAC
french.XCD=East Caribbean Dollar
french.XDR=International Monetary Fund (IMF) Special Drawing Rights
french.XOF=Communaut� Financi�re Africaine (BCEAO) Franc
french.XPF=Comptoirs Fran�ais du Pacifique (CFP) Franc
french.YER=Yemen Rial
french.ZAR=South Africa Rand
french.ZMW=Zambia Kwacha
french.ZWD=Zimbabwe Dollar
;ENGLISH
english.AED=United Arab Emirates Dirham
english.AFN=Afghanistan Afghani
english.ALL=Albania Lek
english.AMD=Armenia Dram
english.ANG=Netherlands Antilles Guilder
english.AOA=Angola Kwanza
english.ARS=Argentina Peso
english.AUD=Australia Dollar
english.AWG=Aruba Guilder
english.AZN=Azerbaijan Manat
english.BAM=Bosnia and Herzegovina Convertible Marka
english.BBD=Barbados Dollar
english.BDT=Bangladesh Taka
english.BGN=Bulgaria Lev
english.BHD=Bahrain Dinar
english.BIF=Burundi Franc
english.BMD=Bermuda Dollar
english.BND=Brunei Darussalam Dollar
english.BOB=Bolivia Bol�viano
english.BRL=Brazil Real
english.BSD=Bahamas Dollar
english.BTN=Bhutan Ngultrum
english.BWP=Botswana Pula
english.BYN=Belarus Ruble
english.BZD=Belize Dollar
english.CAD=Canada Dollar
english.CDF=Congo/Kinshasa Franc
english.CHF=Switzerland Franc
english.CLP=Chile Peso
english.CNY=China Yuan Renminbi
english.COP=Colombia Peso
english.CRC=Costa Rica Colon
english.CUC=Cuba Convertible Peso
english.CUP=Cuba Peso
english.CVE=Cape Verde Escudo
english.CZK=Czech Republic Koruna
english.DJF=Djibouti Franc
english.DKK=Denmark Krone
english.DOP=Dominican Republic Peso
english.DZD=Algeria Dinar
english.EGP=Egypt Pound
english.ERN=Eritrea Nakfa
english.ETB=Ethiopia Birr
english.EUR=Euro Member Countries
english.FJD=Fiji Dollar
english.FKP=Falkland Islands (Malvinas) Pound
english.GBP=United Kingdom Pound
english.GEL=Georgia Lari
english.GGP=Guernsey Pound
english.GHS=Ghana Cedi
english.GIP=Gibraltar Pound
english.GMD=Gambia Dalasi
english.GNF=Guinea Franc
english.GTQ=Guatemala Quetzal
english.GYD=Guyana Dollar
english.HKD=Hong Kong Dollar
english.HNL=Honduras Lempira
english.HRK=Croatia Kuna
english.HTG=Haiti Gourde
english.HUF=Hungary Forint
english.IDR=Indonesia Rupiah
english.ILS=Israel Shekel
english.IMP=Isle of Man Pound
english.INR=India Rupee
english.IQD=Iraq Dinar
english.IRR=Iran Rial
english.ISK=Iceland Krona
english.JEP=Jersey Pound
english.JMD=Jamaica Dollar
english.JOD=Jordan Dinar
english.JPY=Japan Yen
english.KES=Kenya Shilling
english.KGS=Kyrgyzstan Som
english.KHR=Cambodia Riel
english.KMF=Comorian Franc
english.KPW=Korea (North) Won
english.KRW=Korea (South) Won
english.KWD=Kuwait Dinar
english.KYD=Cayman Islands Dollar
english.KZT=Kazakhstan Tenge
english.LAK=Laos Kip
english.LBP=Lebanon Pound
english.LKR=Sri Lanka Rupee
english.LRD=Liberia Dollar
english.LSL=Lesotho Loti
english.LYD=Libya Dinar
english.MAD=Morocco Dirham
english.MDL=Moldova Leu
english.MGA=Madagascar Ariary
english.MKD=Macedonia Denar
english.MMK=Myanmar (Burma) Kyat
english.MNT=Mongolia Tughrik
english.MOP=Macau Pataca
english.MRO=Mauritania Ouguiya
english.MUR=Mauritius Rupee
english.MVR=Maldives (Maldive Islands) Rufiyaa
english.MWK=Malawi Kwacha
english.MXN=Mexico Peso
english.MYR=Malaysia Ringgit
english.MZN=Mozambique Metical
english.NAD=Namibia Dollar
english.NGN=Nigeria Naira
english.NIO=Nicaragua Cordoba
english.NOK=Norway Krone
english.NPR=Nepal Rupee
english.NZD=New Zealand Dollar
english.OMR=Oman Rial
english.PAB=Panama Balboa
english.PEN=Peru Sol
english.PGK=Papua New Guinea Kina
english.PHP=Philippines Peso
english.PKR=Pakistan Rupee
english.PLN=Poland Zloty
english.PYG=Paraguay Guarani
english.QAR=Qatar Riyal
english.RON=Romania Leu
english.RSD=Serbia Dinar
english.RUB=Russia Ruble
english.RWF=Rwanda Franc
english.SAR=Saudi Arabia Riyal
english.SBD=Solomon Islands Dollar
english.SCR=Seychelles Rupee
english.SDG=Sudan Pound
english.SEK=Sweden Krona
english.SGD=Singapore Dollar
english.SHP=Saint Helena Pound
english.SLL=Sierra Leone Leone
english.SOS=Somalia Shilling
english.SPL=Seborga Luigino
english.SRD=Suriname Dollar
english.STD=S�o Tom� and Pr�ncipe Dobra
english.SVC=El Salvador Colon
english.SYP=Syria Pound
english.SZL=Swaziland Lilangeni
english.THB=Thailand Baht
english.TJS=Tajikistan Somoni
english.TMT=Turkmenistan Manat
english.TND=Tunisia Dinar
english.TOP=Tonga Pa'anga
english.TRY=Turkey Lira
english.TTD=Trinidad and Tobago Dollar
english.TVD=Tuvalu Dollar
english.TWD=Taiwan New Dollar
english.TZS=Tanzania Shilling
english.UAH=Ukraine Hryvnia
english.UGX=Uganda Shilling
english.USD=United States Dollar
english.UYU=Uruguay Peso
english.UZS=Uzbekistan Som
english.VEF=Venezuela Bol�var
english.VND=Viet Nam Dong
english.VUV=Vanuatu Vatu
english.WST=Samoa Tala
english.XAF=Communaut� Financi�re Africaine (BEAC) CFA Franc�BEAC
english.XCD=East Caribbean Dollar
english.XDR=International Monetary Fund (IMF) Special Drawing Rights
english.XOF=Communaut� Financi�re Africaine (BCEAO) Franc
english.XPF=Comptoirs Fran�ais du Pacifique (CFP) Franc
english.YER=Yemen Rial
english.ZAR=South Africa Rand
english.ZMW=Zambia Kwacha
english.ZWD=Zimbabwe Dollar

[Files]
Source: "innounzip\unzipper.dll"; Flags: dontcopy
Source: "AMS\fake.bat"; Flags: dontcopy

[Code]
procedure unzip(src, target: AnsiString);
external 'unzip@files:unzipper.dll stdcall delayload';

procedure extract(src, target : AnsiString);
begin
  unzip(ExpandConstant(src), ExpandConstant(target));
end;

function runExe(FileName, Param : string) : Boolean;
var
  ResultCode: Integer;
begin
  Result := Exec(ExpandConstant(FileName), ExpandConstant(Param), '', SW_HIDE , ewWaitUntilTerminated, ResultCode);
  if Result then
  begin 
    Result := (ResultCode = 0);
  end
end;

function runMsi(FileName, Param :string) : Boolean;
var
  ResultCode: Integer;
begin
   Result:= ShellExec('', 'msiexec.exe', '/I ' + FileName, Param , SW_SHOWNORMAL, ewWaitUntilTerminated, ResultCode);
   if Result then
   begin 
      Result := (ResultCode = 0);
   end
end;

type
  TBaseName = string;
  TBaseAMO = string;
  TBaseAddress = string;
  TBasePhone = string;
  TBaseFax = string;
  TBaseEmail = string;
  TBaseStoreEmail = string;
  TBaseLogo = string;
  TBaseLogoStamp = string;
  TBaseLogoTempalte = string;
  TBaseInvoiceText = string;
  TBaseCertification = string;
  TBasePrimary = boolean;
  TBase = record
    BaseName : TBaseName;
    BaseAMO : TBaseAMO;
    BaseAddress : TBaseAddress;
    BasePhone : TBasePhone;
    BaseFax : TBaseFax;
    BaseEmail : TBaseEmail;
    BaseStoreEmail : TBaseStoreEmail;
    BaseLogo : TBaseLogo;
    BaseLogoStamp : TBaseLogoStamp;
    BaseLogoTempalte : TBaseLogoTempalte;
    BaseInvoiceText : TBaseInvoiceText;
    BaseCertification : TBaseCertification;
    BasePrimary : TBasePrimary;
  end;
  TBaseList = array of TBase;

procedure AppendBase(var BaseList: TBaseList; const Base : TBase);
var
  BaseListSize :Integer;
begin
  BaseListSize := GetArrayLength(BaseList);
  SetArrayLength(BaseList, BaseListSize + 1);
  BaseList[BaseListSize] := Base;
end;

function FindBaseByName(const BaseList: TBaseList; const name : string) : TBase;
var
   I: Integer;
begin
  for I := 0 to GetArrayLength(BaseList) - 1 do
    if BaseList[I].BaseName = name then
    begin
      Result := BaseList[I];
      Exit;
     end;
end;

function FindBaseId(const BaseList: TBaseList; const name : string) : Integer;
var
   I: Integer;
begin
  for I := 0 to GetArrayLength(BaseList) - 1 do
    if BaseList[I].BaseName = name then
    begin
      Result := I;
    end;
end;

procedure RemoveBase(var BaseList: TBaseList; const name : string);
var
   I: Integer;
begin
   for I := FindBaseId(BaseList, name)+1 to GetArrayLength(BaseList) - 1 do
   begin
      BaseList[I-1] :=  BaseList[I];
   end;
   SetArrayLength(BaseList, GetArrayLength(BaseList) - 1);
end;

procedure UpdateBase(var BaseList: TBaseList; const name : string; const Base : TBase);
var
   I: Integer;
begin
   for I := 0 to GetArrayLength(BaseList) - 1 do
   if BaseList[I].BaseName = name then
   begin
      BaseList[I] := Base;
      Exit;
   end;
end;
 
type
  TKey = string;
  TValue = string;
  TKeyValue = record
    Key: TKey;
    Value: TValue;
  end;
  TKeyValueList = array of TKeyValue;

function TryGetValue(const KeyValueList: TKeyValueList; const Key: TKey; 
  var Value: TValue): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to GetArrayLength(KeyValueList) - 1 do
    if KeyValueList[I].Key = Key then
    begin
      Result := True;
      Value := KeyValueList[I].Value;
      Exit;
    end;
end;


function getCurrencyList() : TKeyValueList;
var
  KeyValueList: TKeyValueList;
begin
  SetArrayLength(KeyValueList, 162);

  KeyValueList[0].Key := 'AED';
  KeyValueList[0].Value := ExpandConstant('{cm:AED}');
  KeyValueList[1].Key := 'AFN';
  KeyValueList[1].Value := ExpandConstant('{cm:AFN}');
  KeyValueList[2].Key := 'ALL';
  KeyValueList[2].Value := ExpandConstant('{cm:ALL}');
  KeyValueList[3].Key := 'AMD';
  KeyValueList[3].Value := ExpandConstant('{cm:AMD}');
  KeyValueList[4].Key := 'ANG';
  KeyValueList[4].Value := ExpandConstant('{cm:ANG}');
  KeyValueList[5].Key := 'AOA';
  KeyValueList[5].Value := ExpandConstant('{cm:AOA}');
  KeyValueList[6].Key := 'ARS';
  KeyValueList[6].Value := ExpandConstant('{cm:ARS}');
  KeyValueList[7].Key := 'AUD';
  KeyValueList[7].Value := ExpandConstant('{cm:AUD}');
  KeyValueList[8].Key := 'AWG';
  KeyValueList[8].Value := ExpandConstant('{cm:AWG}');
  KeyValueList[9].Key := 'AZN';
  KeyValueList[9].Value := ExpandConstant('{cm:AZN}');
  KeyValueList[10].Key := 'BAM';
  KeyValueList[10].Value := ExpandConstant('{cm:BAM}');
  KeyValueList[11].Key := 'BBD';
  KeyValueList[11].Value := ExpandConstant('{cm:BBD}');
  KeyValueList[12].Key := 'BDT';
  KeyValueList[12].Value := ExpandConstant('{cm:BDT}');
  KeyValueList[13].Key := 'BGN';
  KeyValueList[13].Value := ExpandConstant('{cm:BGN}');
  KeyValueList[14].Key := 'BHD';
  KeyValueList[14].Value := ExpandConstant('{cm:BHD}');
  KeyValueList[15].Key := 'BIF';
  KeyValueList[15].Value := ExpandConstant('{cm:BIF}');
  KeyValueList[16].Key := 'BMD';
  KeyValueList[16].Value := ExpandConstant('{cm:BMD}');
  KeyValueList[17].Key := 'BND';
  KeyValueList[17].Value := ExpandConstant('{cm:BND}');
  KeyValueList[18].Key := 'BOB';
  KeyValueList[18].Value := ExpandConstant('{cm:BOB}');
  KeyValueList[19].Key := 'BRL';
  KeyValueList[19].Value := ExpandConstant('{cm:BRL}');
  KeyValueList[20].Key := 'BSD';
  KeyValueList[20].Value := ExpandConstant('{cm:BSD}');
  KeyValueList[21].Key := 'BTN';
  KeyValueList[21].Value := ExpandConstant('{cm:BTN}');
  KeyValueList[22].Key := 'BWP';
  KeyValueList[22].Value := ExpandConstant('{cm:BWP}');
  KeyValueList[23].Key := 'BYN';
  KeyValueList[23].Value := ExpandConstant('{cm:BYN}');
  KeyValueList[24].Key := 'BZD';
  KeyValueList[24].Value := ExpandConstant('{cm:BZD}');
  KeyValueList[25].Key := 'CAD';
  KeyValueList[25].Value := ExpandConstant('{cm:CAD}');
  KeyValueList[26].Key := 'CDF';
  KeyValueList[26].Value := ExpandConstant('{cm:CDF}');
  KeyValueList[27].Key := 'CHF';
  KeyValueList[27].Value := ExpandConstant('{cm:CHF}');
  KeyValueList[28].Key := 'CLP';
  KeyValueList[28].Value := ExpandConstant('{cm:CLP}');
  KeyValueList[29].Key := 'CNY';
  KeyValueList[29].Value := ExpandConstant('{cm:CNY}');
  KeyValueList[30].Key := 'COP';
  KeyValueList[30].Value := ExpandConstant('{cm:COP}');
  KeyValueList[31].Key := 'CRC';
  KeyValueList[31].Value := ExpandConstant('{cm:CRC}');
  KeyValueList[32].Key := 'CUC';
  KeyValueList[32].Value := ExpandConstant('{cm:CUC}');
  KeyValueList[33].Key := 'CUP';
  KeyValueList[33].Value := ExpandConstant('{cm:CUP}');
  KeyValueList[34].Key := 'CVE';
  KeyValueList[34].Value := ExpandConstant('{cm:CVE}');
  KeyValueList[35].Key := 'CZK';
  KeyValueList[35].Value := ExpandConstant('{cm:CZK}');
  KeyValueList[36].Key := 'DJF';
  KeyValueList[36].Value := ExpandConstant('{cm:DJF}');
  KeyValueList[37].Key := 'DKK';
  KeyValueList[37].Value := ExpandConstant('{cm:DKK}');
  KeyValueList[38].Key := 'DOP';
  KeyValueList[38].Value := ExpandConstant('{cm:DOP}');
  KeyValueList[39].Key := 'DZD';
  KeyValueList[39].Value := ExpandConstant('{cm:DZD}');
  KeyValueList[40].Key := 'EGP';
  KeyValueList[40].Value := ExpandConstant('{cm:EGP}');
  KeyValueList[41].Key := 'ERN';
  KeyValueList[41].Value := ExpandConstant('{cm:ERN}');
  KeyValueList[42].Key := 'ETB';
  KeyValueList[42].Value := ExpandConstant('{cm:ETB}');
  KeyValueList[43].Key := 'EUR';
  KeyValueList[43].Value := ExpandConstant('{cm:EUR}');
  KeyValueList[44].Key := 'FJD';
  KeyValueList[44].Value := ExpandConstant('{cm:FJD}');
  KeyValueList[45].Key := 'FKP';
  KeyValueList[45].Value := ExpandConstant('{cm:FKP}');
  KeyValueList[46].Key := 'GBP';
  KeyValueList[46].Value := ExpandConstant('{cm:GBP}');
  KeyValueList[47].Key := 'GEL';
  KeyValueList[47].Value := ExpandConstant('{cm:GEL}');
  KeyValueList[48].Key := 'GGP';
  KeyValueList[48].Value := ExpandConstant('{cm:GGP}');
  KeyValueList[49].Key := 'GHS';
  KeyValueList[49].Value := ExpandConstant('{cm:GHS}');
  KeyValueList[50].Key := 'GIP';
  KeyValueList[50].Value := ExpandConstant('{cm:GIP}');
  KeyValueList[51].Key := 'GMD';
  KeyValueList[51].Value := ExpandConstant('{cm:GMD}');
  KeyValueList[52].Key := 'GNF';
  KeyValueList[52].Value := ExpandConstant('{cm:GNF}');
  KeyValueList[53].Key := 'GTQ';
  KeyValueList[53].Value := ExpandConstant('{cm:GTQ}');
  KeyValueList[54].Key := 'GYD';
  KeyValueList[54].Value := ExpandConstant('{cm:GYD}');
  KeyValueList[55].Key := 'HKD';
  KeyValueList[55].Value := ExpandConstant('{cm:HKD}');
  KeyValueList[56].Key := 'HNL';
  KeyValueList[56].Value := ExpandConstant('{cm:HNL}');
  KeyValueList[57].Key := 'HRK';
  KeyValueList[57].Value := ExpandConstant('{cm:HRK}');
  KeyValueList[58].Key := 'HTG';
  KeyValueList[58].Value := ExpandConstant('{cm:HTG}');
  KeyValueList[59].Key := 'HUF';
  KeyValueList[59].Value := ExpandConstant('{cm:HUF}');
  KeyValueList[60].Key := 'IDR';
  KeyValueList[60].Value := ExpandConstant('{cm:IDR}');
  KeyValueList[61].Key := 'ILS';
  KeyValueList[61].Value := ExpandConstant('{cm:ILS}');
  KeyValueList[62].Key := 'IMP';
  KeyValueList[62].Value := ExpandConstant('{cm:IMP}');
  KeyValueList[63].Key := 'INR';
  KeyValueList[63].Value := ExpandConstant('{cm:INR}');
  KeyValueList[64].Key := 'IQD';
  KeyValueList[64].Value := ExpandConstant('{cm:IQD}');
  KeyValueList[65].Key := 'IRR';
  KeyValueList[65].Value := ExpandConstant('{cm:IRR}');
  KeyValueList[66].Key := 'ISK';
  KeyValueList[66].Value := ExpandConstant('{cm:ISK}');
  KeyValueList[67].Key := 'JEP';
  KeyValueList[67].Value := ExpandConstant('{cm:JEP}');
  KeyValueList[68].Key := 'JMD';
  KeyValueList[68].Value := ExpandConstant('{cm:JMD}');
  KeyValueList[69].Key := 'JOD';
  KeyValueList[69].Value := ExpandConstant('{cm:JOD}');
  KeyValueList[70].Key := 'JPY';
  KeyValueList[70].Value := ExpandConstant('{cm:JPY}');
  KeyValueList[71].Key := 'KES';
  KeyValueList[71].Value := ExpandConstant('{cm:KES}');
  KeyValueList[72].Key := 'KGS';
  KeyValueList[72].Value := ExpandConstant('{cm:KGS}');
  KeyValueList[73].Key := 'KHR';
  KeyValueList[73].Value := ExpandConstant('{cm:KHR}');
  KeyValueList[74].Key := 'KMF';
  KeyValueList[74].Value := ExpandConstant('{cm:KMF}');
  KeyValueList[75].Key := 'KPW';
  KeyValueList[75].Value := ExpandConstant('{cm:KPW}');
  KeyValueList[76].Key := 'KRW';
  KeyValueList[76].Value := ExpandConstant('{cm:KRW}');
  KeyValueList[77].Key := 'KWD';
  KeyValueList[77].Value := ExpandConstant('{cm:KWD}');
  KeyValueList[78].Key := 'KYD';
  KeyValueList[78].Value := ExpandConstant('{cm:KYD}');
  KeyValueList[79].Key := 'KZT';
  KeyValueList[79].Value := ExpandConstant('{cm:KZT}');
  KeyValueList[80].Key := 'LAK';
  KeyValueList[80].Value := ExpandConstant('{cm:LAK}');
  KeyValueList[81].Key := 'LBP';
  KeyValueList[81].Value := ExpandConstant('{cm:LBP}');
  KeyValueList[82].Key := 'LKR';
  KeyValueList[82].Value := ExpandConstant('{cm:LKR}');
  KeyValueList[83].Key := 'LRD';
  KeyValueList[83].Value := ExpandConstant('{cm:LRD}');
  KeyValueList[84].Key := 'LSL';
  KeyValueList[84].Value := ExpandConstant('{cm:LSL}');
  KeyValueList[85].Key := 'LYD';
  KeyValueList[85].Value := ExpandConstant('{cm:LYD}');
  KeyValueList[86].Key := 'MAD';
  KeyValueList[86].Value := ExpandConstant('{cm:MAD}');
  KeyValueList[87].Key := 'MDL';
  KeyValueList[87].Value := ExpandConstant('{cm:MDL}');
  KeyValueList[88].Key := 'MGA';
  KeyValueList[88].Value := ExpandConstant('{cm:MGA}');
  KeyValueList[89].Key := 'MKD';
  KeyValueList[89].Value := ExpandConstant('{cm:MKD}');
  KeyValueList[90].Key := 'MMK';
  KeyValueList[90].Value := ExpandConstant('{cm:MMK}');
  KeyValueList[91].Key := 'MNT';
  KeyValueList[91].Value := ExpandConstant('{cm:MNT}');
  KeyValueList[92].Key := 'MOP';
  KeyValueList[92].Value := ExpandConstant('{cm:MOP}');
  KeyValueList[93].Key := 'MRO';
  KeyValueList[93].Value := ExpandConstant('{cm:MRO}');
  KeyValueList[94].Key := 'MUR';
  KeyValueList[94].Value := ExpandConstant('{cm:MUR}');
  KeyValueList[95].Key := 'MVR';
  KeyValueList[95].Value := ExpandConstant('{cm:MVR}');
  KeyValueList[96].Key := 'MWK';
  KeyValueList[96].Value := ExpandConstant('{cm:MWK}');
  KeyValueList[97].Key := 'MXN';
  KeyValueList[97].Value := ExpandConstant('{cm:MXN}');
  KeyValueList[98].Key := 'MYR';
  KeyValueList[98].Value := ExpandConstant('{cm:MYR}');
  KeyValueList[99].Key := 'MZN';
  KeyValueList[99].Value := ExpandConstant('{cm:MZN}');
  KeyValueList[100].Key := 'NAD';
  KeyValueList[100].Value := ExpandConstant('{cm:NAD}');
  KeyValueList[101].Key := 'NGN';
  KeyValueList[101].Value := ExpandConstant('{cm:NGN}');
  KeyValueList[102].Key := 'NIO';
  KeyValueList[102].Value := ExpandConstant('{cm:NIO}');
  KeyValueList[103].Key := 'NOK';
  KeyValueList[103].Value := ExpandConstant('{cm:NOK}');
  KeyValueList[104].Key := 'NPR';
  KeyValueList[104].Value := ExpandConstant('{cm:NPR}');
  KeyValueList[105].Key := 'NZD';
  KeyValueList[105].Value := ExpandConstant('{cm:NZD}');
  KeyValueList[106].Key := 'OMR';
  KeyValueList[106].Value := ExpandConstant('{cm:OMR}');
  KeyValueList[107].Key := 'PAB';
  KeyValueList[107].Value := ExpandConstant('{cm:PAB}');
  KeyValueList[108].Key := 'PEN';
  KeyValueList[108].Value := ExpandConstant('{cm:PEN}');
  KeyValueList[109].Key := 'PGK';
  KeyValueList[109].Value := ExpandConstant('{cm:PGK}');
  KeyValueList[110].Key := 'PHP';
  KeyValueList[110].Value := ExpandConstant('{cm:PHP}');
  KeyValueList[111].Key := 'PKR';
  KeyValueList[111].Value := ExpandConstant('{cm:PKR}');
  KeyValueList[112].Key := 'PLN';
  KeyValueList[112].Value := ExpandConstant('{cm:PLN}');
  KeyValueList[113].Key := 'PYG';
  KeyValueList[113].Value := ExpandConstant('{cm:PYG}');
  KeyValueList[114].Key := 'QAR';
  KeyValueList[114].Value := ExpandConstant('{cm:QAR}');
  KeyValueList[115].Key := 'RON';
  KeyValueList[115].Value := ExpandConstant('{cm:RON}');
  KeyValueList[116].Key := 'RSD';
  KeyValueList[116].Value := ExpandConstant('{cm:RSD}');
  KeyValueList[117].Key := 'RUB';
  KeyValueList[117].Value := ExpandConstant('{cm:RUB}');
  KeyValueList[118].Key := 'RWF';
  KeyValueList[118].Value := ExpandConstant('{cm:RWF}');
  KeyValueList[119].Key := 'SAR';
  KeyValueList[119].Value := ExpandConstant('{cm:SAR}');
  KeyValueList[120].Key := 'SBD';
  KeyValueList[120].Value := ExpandConstant('{cm:SBD}');
  KeyValueList[121].Key := 'SCR';
  KeyValueList[121].Value := ExpandConstant('{cm:SCR}');
  KeyValueList[122].Key := 'SDG';
  KeyValueList[122].Value := ExpandConstant('{cm:SDG}');
  KeyValueList[123].Key := 'SEK';
  KeyValueList[123].Value := ExpandConstant('{cm:SEK}');
  KeyValueList[124].Key := 'SGD';
  KeyValueList[124].Value := ExpandConstant('{cm:SGD}');
  KeyValueList[125].Key := 'SHP';
  KeyValueList[125].Value := ExpandConstant('{cm:SHP}');
  KeyValueList[126].Key := 'SLL';
  KeyValueList[126].Value := ExpandConstant('{cm:SLL}');
  KeyValueList[127].Key := 'SOS';
  KeyValueList[127].Value := ExpandConstant('{cm:SOS}');
  KeyValueList[128].Key := 'SPL';
  KeyValueList[128].Value := ExpandConstant('{cm:SPL}');
  KeyValueList[129].Key := 'SRD';
  KeyValueList[129].Value := ExpandConstant('{cm:SRD}');
  KeyValueList[130].Key := 'STD';
  KeyValueList[130].Value := ExpandConstant('{cm:STD}');
  KeyValueList[131].Key := 'SVC';
  KeyValueList[131].Value := ExpandConstant('{cm:SVC}');
  KeyValueList[132].Key := 'SYP';
  KeyValueList[132].Value := ExpandConstant('{cm:SYP}');
  KeyValueList[133].Key := 'SZL';
  KeyValueList[133].Value := ExpandConstant('{cm:SZL}');
  KeyValueList[134].Key := 'THB';
  KeyValueList[134].Value := ExpandConstant('{cm:THB}');
  KeyValueList[135].Key := 'TJS';
  KeyValueList[135].Value := ExpandConstant('{cm:TJS}');
  KeyValueList[136].Key := 'TMT';
  KeyValueList[136].Value := ExpandConstant('{cm:TMT}');
  KeyValueList[137].Key := 'TND';
  KeyValueList[137].Value := ExpandConstant('{cm:TND}');
  KeyValueList[138].Key := 'TOP';
  KeyValueList[138].Value := ExpandConstant('{cm:TOP}');
  KeyValueList[139].Key := 'TRY';
  KeyValueList[139].Value := ExpandConstant('{cm:TRY}');
  KeyValueList[140].Key := 'TTD';
  KeyValueList[140].Value := ExpandConstant('{cm:TTD}');
  KeyValueList[141].Key := 'TVD';
  KeyValueList[141].Value := ExpandConstant('{cm:TVD}');
  KeyValueList[142].Key := 'TWD';
  KeyValueList[142].Value := ExpandConstant('{cm:TWD}');
  KeyValueList[143].Key := 'TZS';
  KeyValueList[143].Value := ExpandConstant('{cm:TZS}');
  KeyValueList[144].Key := 'UAH';
  KeyValueList[144].Value := ExpandConstant('{cm:UAH}');
  KeyValueList[145].Key := 'UGX';
  KeyValueList[145].Value := ExpandConstant('{cm:UGX}');
  KeyValueList[146].Key := 'USD';
  KeyValueList[146].Value := ExpandConstant('{cm:USD}');
  KeyValueList[147].Key := 'UYU';
  KeyValueList[147].Value := ExpandConstant('{cm:UYU}');
  KeyValueList[148].Key := 'UZS';
  KeyValueList[148].Value := ExpandConstant('{cm:UZS}');
  KeyValueList[149].Key := 'VEF';
  KeyValueList[149].Value := ExpandConstant('{cm:VEF}');
  KeyValueList[150].Key := 'VND';
  KeyValueList[150].Value := ExpandConstant('{cm:VND}');
  KeyValueList[151].Key := 'VUV';
  KeyValueList[151].Value := ExpandConstant('{cm:VUV}');
  KeyValueList[152].Key := 'WST';
  KeyValueList[152].Value := ExpandConstant('{cm:WST}');
  KeyValueList[153].Key := 'XAF';
  KeyValueList[153].Value := ExpandConstant('{cm:XAF}');
  KeyValueList[154].Key := 'XCD';
  KeyValueList[154].Value := ExpandConstant('{cm:XCD}');
  KeyValueList[155].Key := 'XDR';
  KeyValueList[155].Value := ExpandConstant('{cm:XDR}');
  KeyValueList[156].Key := 'XOF';
  KeyValueList[156].Value := ExpandConstant('{cm:XOF}');
  KeyValueList[157].Key := 'XPF';
  KeyValueList[157].Value := ExpandConstant('{cm:XPF}');
  KeyValueList[158].Key := 'YER';
  KeyValueList[158].Value := ExpandConstant('{cm:YER}');
  KeyValueList[159].Key := 'ZAR';
  KeyValueList[159].Value := ExpandConstant('{cm:ZAR}');
  KeyValueList[160].Key := 'ZMW';
  KeyValueList[160].Value := ExpandConstant('{cm:ZMW}');
  KeyValueList[161].Key := 'ZWD';
  KeyValueList[161].Value := ExpandConstant('{cm:ZWD}');

  Result:= KeyValueList;
 end;