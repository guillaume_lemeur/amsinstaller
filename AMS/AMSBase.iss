[CustomMessages]
french.BasePageTitle=Creation des Bases
french.BasePageText=
french.BasePageLabel =Bases :
french.BasePageAddButton=Ajouter
french.BasePageEditButton=Editer
french.BasePageRemoveButton=Suprimer
french.EditBasePage1Title=
french.EditBasePage1Text=
french.BaseNameLabel=Base name :
french.BaseAMOLabel=AMO :
french.BaseAddressLabel=Address :
french.BasePhoneLabel=Phone :
french.BaseFaxLabel=Fax :
french.BaseEmailLabel=Email :
french.BaseStoreEmailLabel=Store Email :
french.BasePrimaryLabel=PrimaryBase :
french.EditBasePage2Title=
french.EditBasePage2Text=
french.BaseLogoLabel=Logo :
french.BaseLogoStampLabel=Logo Stamp :
french.BaseLogoTemplateLabel=Logo Template :
french.BaseInvoiceLabel=Invoice text :
french.BaseCertificatLabel=Certification :
french.EditBasePageCancelButton=Annuler
french.EditBasePageSaveButton=Sauver
;ENGLISH
english.BasePageTitle=Bases Creation
english.BasePageText=
english.BasePageLabel=Bases :
english.BasePageAddButton=Add
english.BasePageEditButton=Edit
english.BasePageRemoveButton=Remove
english.EditBasePage1Title=
english.EditBasePage1Text=
english.BaseNameLabel=Base name :
english.BaseAMOLabel=AMO :
english.BaseAddressLabel=Address :
english.BasePhoneLabel=Phone :
english.BaseFaxLabel=Fax :
english.BaseEmailLabel=Email :
english.BaseStoreEmailLabel=Store Email :
english.BasePrimaryLabel=PrimaryBase :
english.EditBasePage2Title=
english.EditBasePage2Text=
english.BaseLogoLabel=Logo :
english.BaseLogoStampLabel=Logo Stamp :
english.BaseLogoTemplateLabel=Logo Template :
english.BaseInvoiceLabel=Invoice text :
english.BaseCertificatLabel=Certification :
english.EditBasePageCancelButton=Cancel
english.EditBasePageSaveButton=Save 


[code]
var
  BasePage : TWizardPage;
  EditBasePage1 : TWizardPage;
  EditBasePage2 : TWizardPage;
  BasePageBaseList : TNewListBox;
  //Base Fields
  BaseNameEdit : TNewEdit;
  AMOEdit : TNewEdit;
  AddressMemo  : TNewMemo;
  PhoneEdit  : TNewEdit;
  FaxEdit  : TNewEdit;
  EmailEdit  : TNewEdit;
  StoreEmailEdit  : TNewEdit;
  PrimaryCheckBox  : TNewCheckBox;
  LogoEdit  : TNewEdit;
  LogoStamEdit  : TNewEdit;
  LogoTemplateEdit  : TNewEdit;
  InvoiceTextMemo   : TNewMemo;
  CertificatMemo  : TNewMemo;

  BaseList : TBaseList;  
  isBaseCreate : Boolean;
  isBaseEdit : Boolean;

procedure CleanEditPages();
begin
  BaseNameEdit.Text :='';
  AMOEdit.Text :='';
  AddressMemo.Text :='';
  PhoneEdit.Text :='';
  FaxEdit.Text :='';
  EmailEdit.Text :='';
  StoreEmailEdit.Text :='';
  LogoEdit.Text :='';
  LogoStamEdit.Text :='';
  LogoTemplateEdit.Text :='';
  InvoiceTextMemo.Text :='';
  CertificatMemo.Text :='';
  PrimaryCheckBox.Checked :=False;
end;

procedure FillEditPages();
var
  Base : TBase;
begin
  Base := FindBaseByName(BaseList, BasePageBaseList.Items[BasePageBaseList.ItemIndex])
  BaseNameEdit.Text := Base.BaseName;
  AMOEdit.Text :=Base.BaseAMO;
  AddressMemo.Text :=Base.BaseAddress;
  PhoneEdit.Text :=Base.BasePhone;
  FaxEdit.Text :=Base.BaseFax;
  EmailEdit.Text :=Base.BaseEmail;
  StoreEmailEdit.Text :=Base.BaseStoreEmail;
  LogoEdit.Text :=Base.BaseLogo;
  LogoStamEdit.Text :=Base.BaseLogoStamp;
  LogoTemplateEdit.Text :=Base.BaseLogoTempalte;
  InvoiceTextMemo.Text :=Base.BaseInvoiceText;
  CertificatMemo.Text :=Base.BaseCertification;
  PrimaryCheckBox.Checked :=Base.BasePrimary;
end;

function BuildBase() : TBase;
var
  Base : TBase;
begin
  Base.BaseName := BaseNameEdit.Text;
  Base.BaseAMO := AMOEdit.Text;
  Base.BaseAddress := AddressMemo.Text;
  Base.BasePhone := PhoneEdit.Text;
  Base.BaseFax := FaxEdit.Text;
  Base.BaseEmail := EmailEdit.Text;
  Base.BaseStoreEmail := StoreEmailEdit.Text;
  Base.BaseLogo := LogoEdit.Text;
  Base.BaseLogoStamp := LogoStamEdit.Text;
  Base.BaseLogoTempalte := LogoTemplateEdit.Text;
  Base.BaseInvoiceText := InvoiceTextMemo.Text;
  Base.BaseCertification := CertificatMemo.Text;
  Base.BasePrimary := PrimaryCheckBox.Checked;

  Result := Base;
end;

procedure BasePageAddButtonClicked(Sender: TObject);
begin
     isBaseCreate := True;
     Wizardform.NextButton.Enabled := True;
     ClickNext();
end;

procedure BasePageRemoveButtonClicked(Sender: TObject);
begin
   RemoveBase(BaseList,BasePageBaseList.Items[BasePageBaseList.ItemIndex]);
   BasePageBaseList.Items.Delete(BasePageBaseList.ItemIndex);
end;


procedure BasePageEditButtonClicked(Sender: TObject);
begin
     isBaseEdit := True;
     Wizardform.NextButton.Enabled := True;
     FillEditPages();
     ClickNext();
end;

procedure SaveBaseButtonClicked(Sender: TObject);
Var
  Base : TBase;
  BaseListSize : Integer;
begin
  if isBaseCreate then
    begin
      Base:= BuildBase();
      AppendBase(BaseList,Base);
      BaseListSize := GetArrayLength(BaseList);
      BasePageBaseList.Items.Add(BaseList[BaseListSize-1].BaseName);
    end
  else if isBaseEdit then
    begin
      Base:= BuildBase();
      UpdateBase(BaseList, BasePageBaseList.Items[BasePageBaseList.ItemIndex], Base);
      BasePageBaseList.Items[BasePageBaseList.ItemIndex] := Base.BaseName;
    end;
  isBaseEdit := False
  isBaseCreate := False
  ClickBack();
  CleanEditPages();
end;

procedure CancelBaseButtonClicked(Sender: TObject);
begin
  isBaseEdit := False
  isBaseCreate := False
  ClickBack();
  CleanEditPages();
end;

procedure CreateBasePage;
var
  I:Integer;
  BasePageLabel : TLabel;
  BasePageAddButton : TNewButton;
  BasePageEditButton : TNewButton;
  BasePageRemoveButton : TNewButton;
begin
  BasePage :=  CreateCustomPage(PasswordPage.ID, ExpandConstant('{cm:BasePageTitle}'), ExpandConstant('{cm:BasePageText}'));

  BasePageLabel := TLabel.Create(BasePage);
  BasePageLabel.Caption := ExpandConstant('{cm:BasePageLabel}');
  BasePageLabel.Parent := BasePage.Surface;

  BasePageBaseList := TNewListBox.Create(BasePage);
  BasePageBaseList.Top := BasePageLabel.Top + BasePageLabel.Height + ScaleY(4);
  BasePageBaseList.Width := BasePage.SurfaceWidth - ScaleX(250)
  BasePageBaseList.Height := BasePage.SurfaceHeight - ScaleY(20)
  BasePageBaseList.Parent := BasePage.Surface;

  BasePageAddButton := TNewButton.create(BasePage);
  BasePageAddButton.Caption:= ExpandConstant('{cm:BasePageAddButton}');
  BasePageAddButton.Top := BasePageBaseList.Top + BasePageLabel.Height + BasePageBaseList.Height/5;
  BasePageAddButton.Left := BasePageBaseList.Left + BasePageBaseList.Width + ScaleX(20);
  BasePageAddButton.Width := ScaleX(50);
  BasePageAddButton.Height := ScaleY(25);
  BasePageAddButton.OnClick := @BasePageAddButtonClicked;
  BasePageAddButton.Parent := BasePage.Surface;

  BasePageEditButton := TNewButton.create(BasePage);
  BasePageEditButton.Caption:= ExpandConstant('{cm:BasePageEditButton}');
  BasePageEditButton.Top := BasePageAddButton.Top + ScaleY(30)
  BasePageEditButton.Left := BasePageAddButton.Left;
  BasePageEditButton.Width := ScaleX(50);
  BasePageEditButton.Height := ScaleY(25);
  BasePageEditButton.OnClick := @BasePageEditButtonClicked;
  BasePageEditButton.Parent := BasePage.Surface;

  BasePageRemoveButton := TNewButton.create(BasePage);
  BasePageRemoveButton.Caption:= ExpandConstant('{cm:BasePageRemoveButton}');
  BasePageRemoveButton.Top := BasePageEditButton.Top + ScaleY(30)
  BasePageRemoveButton.Left := BasePageEditButton.Left;
  BasePageRemoveButton.Width := ScaleX(50);
  BasePageRemoveButton.Height := ScaleY(25);
  BasePageRemoveButton.OnClick := @BasePageRemoveButtonClicked;
  BasePageRemoveButton.Parent := BasePage.Surface;

  for I:= 0 to GetArrayLength(BaseList)-1 do
  begin
      BasePageBaseList.Items.Add(BaseList[I].BaseName);
  end;
end;

procedure CreateEditBasePage1;
var 
    BaseNameLabel : TLabel; 
    AMOLabel : TLabel;
    AddressLabel : TLabel;
    PhoneLabel : TLabel;
    FaxLabel : TLabel; 
    EmailLabel : TLabel;
    StoreEmailLabel : TLabel;
    PrimaryLabel  : TLabel;
begin
    EditBasePage1 :=  CreateCustomPage(BasePage.ID, ExpandConstant('{cm:EditBasePage1Title}'), ExpandConstant('{cm:EditBasePage1Text}'));

    BaseNameLabel := TLabel.Create(EditBasePage1);
    BaseNameLabel.Caption := ExpandConstant('{cm:BaseNameLabel}');
    BaseNameLabel.Alignment := taLeftJustify;
    BaseNameLabel.Parent := EditBasePage1.Surface;

    BaseNameEdit := TNewEdit.Create(EditBasePage1);
    BaseNameEdit.Top := BaseNameLabel.Top;
    BaseNameEdit.Left :=  BaseNameLabel.Left + BaseNameLabel.Width + ScaleX(10);
    BaseNameEdit.Width := ScaleX(100)
    BaseNameEdit.Parent := EditBasePage1.Surface;

    AMOLabel := TLabel.Create(EditBasePage1);
    AMOLabel.Top := BaseNameLabel.TOP + BaseNameLabel.Height + ScaleY(10);
    AMOLabel.Width := BaseNameLabel.Width;
    AMOLabel.Caption := ExpandConstant('{cm:BaseAMOLabel}');
    AMOLabel.Alignment := taRightJustify;
    AMOLabel.Parent := EditBasePage1.Surface;

    AMOEdit:= TNewEdit.Create(EditBasePage1);
    AMOEdit.Top := AMOLabel.Top;
    AMOEdit.Left := BaseNameEdit.Left;
    AMOEdit.Width := ScaleX(70)
    AMOEdit.Parent := EditBasePage1.Surface;

    AddressLabel := TLabel.Create(EditBasePage1);
    AddressLabel.Top := AMOLabel.TOP + AMOLabel.Height + ScaleY(10);
    AddressLabel.Caption := ExpandConstant('{cm:BaseAddressLabel}');
    AddressLabel.Alignment := taLeftJustify;
    AddressLabel.Parent := EditBasePage1.Surface;

    AddressMemo:= TNewMemo.Create(EditBasePage1);
    AddressMemo.Top := AddressLabel.Top;
    AddressMemo.Left := BaseNameEdit.Left;
    AddressMemo.Width := ScaleX(200);
    AddressMemo.ScrollBars := ssVertical;
    AddressMemo.Parent := EditBasePage1.Surface;

    PhoneLabel := TLabel.Create(EditBasePage1);
    PhoneLabel.Top := AddressLabel.TOP + AddressMemo.Height + ScaleY(10);
    PhoneLabel.Caption := ExpandConstant('{cm:BasePhoneLabel}');
    PhoneLabel.Alignment := taLeftJustify;
    PhoneLabel.Parent := EditBasePage1.Surface;

    PhoneEdit:= TNewEdit.Create(EditBasePage1);
    PhoneEdit.Top := PhoneLabel.Top;
    PhoneEdit.Left := BaseNameEdit.Left;
    PhoneEdit.Width := ScaleX(110)
    PhoneEdit.Parent := EditBasePage1.Surface;

    FaxLabel:= TLabel.Create(EditBasePage1);
    FaxLabel.Top := PhoneLabel.TOP + PhoneLabel.Height + ScaleY(10);
    FaxLabel.Caption := ExpandConstant('{cm:BaseFaxLabel}');
    FaxLabel.Alignment := taLeftJustify;
    FaxLabel.Parent := EditBasePage1.Surface;

    FaxEdit:= TNewEdit.Create(EditBasePage1);
    FaxEdit.Top := FaxLabel.Top;
    FaxEdit.Left := BaseNameEdit.Left;
    FaxEdit.Width := ScaleX(110)
    FaxEdit.Parent := EditBasePage1.Surface;

    EmailLabel := TLabel.Create(EditBasePage1);
    EmailLabel.Top := FaxLabel.TOP + FaxLabel.Height + ScaleY(10);
    EmailLabel.Caption := ExpandConstant('{cm:BaseEmailLabel}');
    EmailLabel.Alignment := taLeftJustify;
    EmailLabel.Parent := EditBasePage1.Surface;
    
    EmailEdit  := TNewEdit.Create(EditBasePage1);
    EmailEdit.Top := EmailLabel.Top;
    EmailEdit.Left := BaseNameEdit.Left;
    EmailEdit.Width := ScaleX(200)
    EmailEdit.Parent := EditBasePage1.Surface;

    StoreEmailLabel := TLabel.Create(EditBasePage1);
    StoreEmailLabel.Top := EmailLabel.TOP + EmailLabel.Height + ScaleY(10);
    StoreEmailLabel.Caption := ExpandConstant('{cm:BaseStoreEmailLabel}');
    StoreEmailLabel.Alignment := taLeftJustify;
    StoreEmailLabel.Parent := EditBasePage1.Surface;

    StoreEmailEdit  := TNewEdit.Create(EditBasePage1);
    StoreEmailEdit.Top := StoreEmailLabel.Top;
    StoreEmailEdit.Left := BaseNameEdit.Left;
    StoreEmailEdit.Width := ScaleX(200)
    StoreEmailEdit.Parent := EditBasePage1.Surface;

    PrimaryLabel := TLabel.Create(EditBasePage1);
    PrimaryLabel.Top := StoreEmailLabel.TOP + StoreEmailLabel.Height + ScaleY(10);
    PrimaryLabel.Caption := ExpandConstant('{cm:BasePrimaryLabel}');
    PrimaryLabel.Alignment := taLeftJustify;
    PrimaryLabel.Parent := EditBasePage1.Surface;

    PrimaryCheckBox  := TNewCheckBox.Create(EditBasePage1);
    PrimaryCheckBox.Top := PrimaryLabel.Top;
    PrimaryCheckBox.Height := PrimaryLabel.Height; 
    PrimaryCheckBox.Left := BaseNameEdit.Left;  
    PrimaryCheckBox.Parent := EditBasePage1.Surface;
end;

procedure CreateEditBasePage2;
var
  LogoLabel : TLabel;
  LogoStampLabel : TLabel;
  LogoTemplateLabel : TLabel;
  InvoiceTextLabel  : TLabel; 
  CertificatLabel : TLabel;
  CancelButton : TNewButton;
  SaveButton : TNewButton;
begin
    EditBasePage2 :=  CreateCustomPage(EditBasePage1.ID, ExpandConstant('{cm:EditBasePage2Title}'), ExpandConstant('{cm:EditBasePage2Text}'));

    LogoLabel := TLabel.Create(EditBasePage2);
    LogoLabel.Caption := ExpandConstant('{cm:BaseLogoLabel}');
    LogoLabel.Parent := EditBasePage2.Surface;

    LogoEdit := TNewEdit.Create(EditBasePage2);
    LogoEdit.Top := LogoLabel.Top;
    LogoEdit.Left :=  LogoLabel.Left + ScaleX(80);
    LogoEdit.Width := ScaleX(200)
    LogoEdit.Parent := EditBasePage2.Surface;

    LogoStampLabel := TLabel.Create(EditBasePage2);
    LogoStampLabel.Top := LogoLabel.TOP + LogoLabel.Height + ScaleY(10);
    LogoStampLabel.Caption := ExpandConstant('{cm:BaseLogoStampLabel}');
    LogoStampLabel.Parent := EditBasePage2.Surface;

    LogoStamEdit:= TNewEdit.Create(EditBasePage2);
    LogoStamEdit.Top := LogoStampLabel.Top;
    LogoStamEdit.Left :=  LogoEdit.Left;
    LogoStamEdit.Width := ScaleX(200)
    LogoStamEdit.Parent := EditBasePage2.Surface;

    LogoTemplateLabel := TLabel.Create(EditBasePage2);
    LogoTemplateLabel.Top := LogoStampLabel.TOP + LogoStampLabel.Height + ScaleY(10);
    LogoTemplateLabel.Caption := ExpandConstant('{cm:BaseLogoTemplateLabel}');
    LogoTemplateLabel.Parent := EditBasePage2.Surface;

    LogoTemplateEdit:= TNewEdit.Create(EditBasePage2);
    LogoTemplateEdit.Top := LogoTemplateLabel.Top;
    LogoTemplateEdit.Left :=  LogoEdit.Left;
    LogoTemplateEdit.Width := ScaleX(200)
    LogoTemplateEdit.Parent := EditBasePage2.Surface;

    InvoiceTextLabel := TLabel.Create(EditBasePage2);
    InvoiceTextLabel.Top := LogoTemplateLabel.TOP + LogoTemplateLabel.Height + ScaleY(10);
    InvoiceTextLabel.Caption := ExpandConstant('{cm:BaseInvoiceLabel}');
    InvoiceTextLabel.Parent := EditBasePage2.Surface;

    InvoiceTextMemo:= TNewMemo.Create(EditBasePage2);
    InvoiceTextMemo.Top := InvoiceTextLabel.Top;
    InvoiceTextMemo.Left :=  LogoEdit.Left;
    InvoiceTextMemo.Width := ScaleX(200);
    InvoiceTextMemo.ScrollBars := ssVertical;
    InvoiceTextMemo.Parent := EditBasePage2.Surface;

    CertificatLabel := TLabel.Create(EditBasePage2);
    CertificatLabel.Top := InvoiceTextLabel.TOP + InvoiceTextMemo.Height + ScaleY(10);
    CertificatLabel.Caption := ExpandConstant('{cm:BaseCertificatLabel}');
    CertificatLabel.Parent := EditBasePage2.Surface;

    CertificatMemo:= TNewMemo.Create(EditBasePage2);
    CertificatMemo.Top := CertificatLabel.Top;
    CertificatMemo.Left :=  LogoEdit.Left;
    CertificatMemo.Width := ScaleX(200);
    CertificatMemo.ScrollBars := ssVertical;
    CertificatMemo.Parent := EditBasePage2.Surface;
  
    CancelButton := TNewButton.create(EditBasePage2);
    CancelButton.Caption:= ExpandConstant('{cm:EditBasePageCancelButton}');
    CancelButton.Top := CertificatLabel.Top + + CertificatMemo.Height + ScaleY(10);
    CancelButton.Left := EditBasePage2.SurfaceWidth - ScaleX(60);
    CancelButton.Width := ScaleX(50);
    CancelButton.Height := ScaleY(25);
    CancelButton.OnClick := @CancelBaseButtonClicked;
    CancelButton.Parent := EditBasePage2.Surface;
    
    SaveButton := TNewButton.create(EditBasePage2);
    SaveButton.Caption:= ExpandConstant('{cm:EditBasePageSaveButton}');
    SaveButton.Top := CertificatLabel.Top + + CertificatMemo.Height + ScaleY(10);
    SaveButton.Left := CancelButton.Left - CancelButton.Width - ScaleX(10);
    SaveButton.Width := ScaleX(50);
    SaveButton.Height := ScaleY(25);
    SaveButton.OnClick := @SaveBaseButtonClicked;
    SaveButton.Parent := EditBasePage2.Surface;
    
   
end;