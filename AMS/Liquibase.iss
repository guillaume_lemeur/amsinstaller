[CustomMessages]
french.DataBaseServerPageTitle=Param�tres du Server de base de donn�es
french.DataBaseServerPageText=Veuillez fournir les informations suivante pour se connecter au server de base de donn�es
french.LiquibaseHostLabel=Host
french.LiquibasePortLabel=Port

english.DataBaseServerPageTitle=Database Server Settings
english.DataBaseServerPageText=Please provide information to connect to your database server.
english.LiquibaseHostLabel=Host
english.LiquibasePortLabel=Port

[Files]
Source: "{#Delivery}\ams_liquibase\*"; DestDir: "{tmp}\Liquibase"; Flags: recursesubdirs createallsubdirs; Check: needLiquibase;

[Run]
;Create Update Liquibase
Filename: "fake.bat";  StatusMsg: {cm:updateDataBase}; Flags: runhidden; AfterInstall: runLiquibase() ; Check: needLiquibase;

[Code]
var
  LiquibaseHostEdit : TNewEdit;
  LiquibasePortEdit : TNewEdit;
  DataBaseServerPage : TWizardPage;

function needLiquibase(): Boolean;
begin
  if (InstallationType = ExpandConstant('{cm:All}')) or (InstallationType = ExpandConstant('{cm:Server}')) then
  begin
    Result:=True;
  end else 
  begin
    Result:=False;
  end
 end;

procedure runLiquibase();
begin
  if not runExe('{tmp}\Liquibase\ams_liquibase.bat','--action=update --port={code:GetLiquibasePort} --host={code:GetLiquibaseHost}') then
    begin
      MsgBox(ExpandConstant('{cm:ErrorLiquibase}'), mbCriticalError, MB_OK);
      Abort := True;
    end
end;

function GetLiquibasePort() : String;
begin
  Result := LiquibasePortEdit.Text;
end;

function GetLiquibaseHost() : String;
begin
  Result:= LiquibaseHostEdit.Text;
end;

procedure CreateDataBaseServerPage();
var
  LiquibaseHostLabel : TLabel;
  LiquibasePortLabel : TLabel;
begin
  DataBaseServerPage := CreateCustomPage(wpPreparing, ExpandConstant('{cm:DataBaseServerPageTitle}'), ExpandConstant('{cm:DataBaseServerPageText}'));

  LiquibaseHostLabel := TLabel.Create(DataBaseServerPage);
  LiquibaseHostLabel.Caption := ExpandConstant('{cm:LiquibaseHostLabel}');
  LiquibaseHostLabel.Parent := DataBaseServerPage.Surface;
  
  LiquibaseHostEdit := TNewEdit.Create(DataBaseServerPage);
  LiquibaseHostEdit.Top := LiquibaseHostLabel.Top + LiquibaseHostLabel.Height + ScaleY(4);
  LiquibaseHostEdit.Width := DataBaseServerPage.SurfaceWidth - ScaleX(200);
  LiquibaseHostEdit.Parent := DataBaseServerPage.Surface;
  LiquibaseHostEdit.text := '127.0.0.1'

  LiquibasePortLabel := TLabel.Create(DataBaseServerPage);
  LiquibasePortLabel.Top := LiquibaseHostEdit.Top + LiquibaseHostEdit.Height + ScaleY(8);
  LiquibasePortLabel.Caption := ExpandConstant('{cm:LiquibasePortLabel}');
  LiquibasePortLabel.Parent := DataBaseServerPage.Surface;
  
  LiquibasePortEdit := TNewEdit.Create(DataBaseServerPage);
  LiquibasePortEdit.Top := LiquibasePortLabel.Top + LiquibasePortLabel.Height + ScaleY(4);
  LiquibasePortEdit.Width := DataBaseServerPage.SurfaceWidth - ScaleX(200);
  LiquibasePortEdit.Parent := DataBaseServerPage.Surface;
  LiquibasePortEdit.Text := '3307';

end;